<?php
include("../services/auth.php");
?>

 <?php
                if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'ExamAdministrator') !== false){
            ?>
<div class="col-lg-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            New Question Set
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
         <form enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form">
         <div class="form-group">
                <label class="control-label" for="inputError">Question Set Name</label>
          <input id="setName" type="text" class="form-control" placeholder="Question Set Name" ng-model="setName" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Question Set Description</label>
          <textarea id="setDesc" type="text" class="form-control" placeholder="Question Set Description" ng-model="setDesc" data-ng-required="true" col="3"></textarea>
        </div>
        <input style="margin-top: 10px;padding:7px; width:130px" class="btn btn-success" type="submit" value="Submit" ng-click="form.$valid && SaveQuestionSet()"/>
       </form>  
       </div>
    </div>
    
    <div class="panel panel-default">
            <div class="panel-heading">
                Registered Question Sets
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                    <thead>
                        <tr>
                         <td>Set Id</td>
                         <td>Question Set Name</td>
                         <td>Question Set Description</td>
                         <td>Action</td>
                   </tr>
                 </thead>
                 <tbody>
                     <tr ng-repeat="row in sets">
                         <td>{{row.id}}</td>
                         <td>{{row.questionSet}}</td>
                         <td>{{row.Description}}</td>
                         <td style="white-space: nowrap">
                            <a class="btn btn-danger" type="button" name="delete" ng-click="deleteSet(row.id)"><i class="fa fa-trash"></i></a>
                         </td>
                     </tr>
                 </tbody>
            </table>
          </div> 
    </div>
</div>

<?php
                }
            ?>