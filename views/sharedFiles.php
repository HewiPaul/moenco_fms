<?php
include("../services/auth.php");
?>

<div style="padding: 10px;" class="col-lg-12">
<div id="{{cata.id}}" class="panel-group wow fadeInUpBig" data-wow-delay="0.2s" ng-repeat="cata in catas">
    <div class="panel panel-default">
            <a style="text-decoration:none; width: 100%; padding: 20px" class="btn btn-link panel-heading" data-toggle="collapse" data-parent="#{{cata.id}}" href=".{{cata.id}}">
                    <div class="panel-heading">
                        <div style="float: left">
                                <img class="img img-responsive" src="images/moenco.png" width="50" height="50" />
                        </div>
                        <div style="float: left; width: 95%">
                        <div style="padding: 5px;" class="panel-title">
                            <h5 style="float: left;">{{cata.name}}</h5>
                            <i style="font-size: x-large; float:right" class="fa fa-angle-down"></i>
                        </div>
                        </div>
                    </div>
            </a>
        <div class="{{cata.id}}" class="panel-collapse collapse">
            <div class="panel-body bg-info">
                <div style="padding: 10px" class="row">
                    <div style="width: 100%" class="row">
                    <form style="float:right;" class="form-inline">
                        <div class="row align-items-center">
                            <div class="col text-right">
                                <div class="input-group">
                                    <div style="padding-right: 35px;" class="row">
                                    <input style="float: right;" id="search" class="form-control" type="text" ng-model="search" placeholder="&#128269; Search" type="search"/>
                                    </div>
                                </div>
                                </div>
                          </div>
                    </form>
                    </div>
                    <div style="padding: 5px;" class="col-md-12">
                        <div style="padding-bottom: 10px; float:left" class="col-md-3" ng-repeat="file in files_ | filter: { catagory: cata.name} | filter: { name: search}">
                                <div class="panel panel-default text-center">
                                    <div class="panel-heading">
                                        <span class="fa-stack fa-3x">
                                            <img class="img img-responsive" src="images/file2.png">
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <h6>{{file.name}}</h6>
                                        <a href="#/viewFile" ng-click="setFileValues(file.directory,file.name,file.pages,file.base64File)" class="btn btn-primary">View</a>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </div></div>
</div>