<?php
include("../services/auth.php");
?>

<link rel="stylesheet" href="dist/css/select2.css">

                <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'FileAdministrator') !== false){
                ?>   

<div class="col-lg-12">
    <h2>File Upload.</h2>
    <div class="panel panel-default">
        <div class="panel-heading">
            Add File
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
        <form id="frmUploader" enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form" ng-submit="SaveFile()">
            <div class="form-group">
            <label class="control-label" for="inputError">File</label>
            <input id="file1" type="file"
                ng-model="file1" accept=".htm,.html,.pdf,application/html,application/pdf" onchange="angular.element(this).scope().uploadedFile(this)" class="form-control">
            </div>
            <!--accept="image/*"-->
            <div class="form-group">
                <label class="control-label" for="inputError">Directory</label>
                <input class="form-control" id="dir" ng-model="dir" type="text" placeholder="Directory" required/>
            </div>
            <div class="form-group">
                <label class="control-label" for="inputError">File Name</label>
                <input class="form-control" id="name" ng-model="name" type="text" placeholder="File Name" required/>
            </div>

            <div class="form-group">
                <label class="control-label" for="inputError">Catagory</label>
                <select id="catagory" class="js-example-basic-single form-control"
                      name="state" class="form-control" ng-model="catagory" style="width:100%"
                      required>
                      <option value="" selected disabled hidden>Choose here</option>
                      <option value="{{cat.name}}" ng-repeat="cat in catagories_">{{cat.name}}</option>
                </select>
           </div>

           <div class="form-group">
            <label class="control-label" for="inputError">Number Of Pages</label>
            <input class="form-control" id="pages" ng-model="pages" type="text" placeholder="Number Of Pages" required/>
           </div>

            <!--<div class="form-group">
                <label class="control-label" for="inputError">Security</label>
                <select id="security" ng-model="security" style="width:100%" class="js-example-basic-single form-control" data-ng-required="true">
                    <option value="" selected disabled hidden>Choose Here</option>
                    <option value="0">Open</option>
                    <option value="1">Secured</option>
                </select>
            </div>-->
            <input style="margin-top: 10px;padding:7px; width:130px" class="btn btn-success" type="submit" value="Submit"/>
            
        </form>
        </div>
     </div>

     <div class="panel panel-default">
        <div class="panel-heading">
            Uploaded Files
        </div>
        <!-- /.panel-heading -->
        <div style="overflow-x: auto;" class="panel-body">
        <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                <thead>
                    <tr>
                     <td>File Id</td>
                     <td>File Name</td>
                     <td>Number Of Page</td>
                     <td>Catagory</td>
                     <td>Security</td>
                     <td>Action</td>
               </tr>
             </thead>
             <tbody>
                 <tr ng-repeat="row in files_">
                     <td>{{row.id}}</td>
                     <td>{{row.name}}</td>
                     <td>{{row.pages}}</td>
                     <td>{{row.catagory}}</td>
                     <td>{{row.security}}</td>
                     <td style="white-space: nowrap">
                     <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#edit" ng-click="FetchDescription(row.id)"><i class="fa fa-edit"></i></button></a>
                     <a class="btn btn-danger" type="button" name="delete" ng-click="deleteFile(row.id)"><i class="fa fa-trash"></i></a>
                     </td>
                 </tr>
             </tbody>
        </table>
      </div> 
</div>
</div>




<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit File Info.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit File Info.</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
                <div class="form-group">
                    <input id="id" type="hidden" ng-model="id"/>
                    <label class="control-label" for="inputError">Directory</label>
                    <input id="dir2" class="form-control" placeholder="Directory" type="text" ng-model="dir2" required>
                </div>
                <div c
                lass="form-group">
                     <label class="control-label" for="inputError">File Name</label>
                    <input id="name2" class="form-control" placeholder="File Name" type="text" ng-model="name2" required>
                </div>
                <div class="form-group">
                <label class="control-label" for="inputError">Catagory</label>
                <select id="catagory2" class="js-example-basic-single form-control"
                      name="state" class="form-control" ng-model="catagory2" style="width:100%"
                      required>
                      <option value="" selected disabled hidden>Choose here</option>
                      <option value="{{cat.name}}" ng-repeat="cat in catagories_">{{cat.name}}</option>
                </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="inputError">Number Of Pages</label>
                    <input class="form-control" id="pages2" ng-model="pages2" type="text" placeholder="Number Of Pages" required/>
                </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateFile(id)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

                <?php
                    }
                ?>

<script src="dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $("#catagory").select2();
    $('.js-example-basic-single').select2();
});