<?php
include("../services/auth.php");
?>

<link rel="stylesheet" href="dist/css/select2.css">

            <?php
                if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'ExamAdministrator') !== false){
            ?>

<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            New Question
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
         <form enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form">
         <div class="form-group">
                <input id="userName" style="visibility:hidden; display:none" value="<?php echo $_SESSION['fsm_username']; ?>" />
                <label class="control-label" for="inputError">Question Set</label>
                <select id="set" class="js-example-basic-single form-control"
                      name="set" class="form-control" ng-model="set" style="width:100%"
                      required>
                      <option value="" selected disabled hidden>Choose here</option>
                      <option value="{{set.id}}" ng-repeat="set in sets">{{set.questionSet}}</option>
                </select>
           </div>
            <div class="form-group">
                <label class="control-label" for="inputError">Question</label>
          <textarea id="question" type="text" class="form-control" placeholder="Question" ng-model="question" data-ng-required="true" rows="6" required></textarea>
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Choice A</label>
          <input id="A" type="text"  class="form-control" placeholder="Choice A" ng-model="A" required>
         </div>
         <div class="form-group">
                <label class="control-label" for="inputError">Choice B</label>
          <input id="B" type="text"  class="form-control" placeholder="Choice B" ng-model="B" required>
         </div>
         <div class="form-group">
                <label class="control-label" for="inputError">Choice C</label>
          <input id="C" type="text"  class="form-control" placeholder="Choice C" ng-model="C" required>
         </div>
         <div class="form-group">
                <label class="control-label" for="inputError">Choice D</label>
          <input id="D" type="text"  class="form-control" placeholder="Choice D" ng-model="D" required>
         </div>
         <div class="form-group">
                <label class="control-label" for="inputError">Choice E</label>
          <input id="E" type="text"  class="form-control" placeholder="Choice E" ng-model="E">
         </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Answer</label>
                <select id="answer" class="js-example-basic-single form-control"
                      name="answer" class="form-control" ng-model="answer" style="width:100%"
                      required>
                      <option value="" selected disabled hidden>Choose here</option>
                      <option value="A">A</option>
                      <option value="B">B</option>
                      <option value="C">C</option>
                      <option value="D">D</option>
                      <option value="E">E</option>
                </select>
           </div>
           <div class="form-group">
                <label class="control-label" for="inputError">Remark</label>
          <textarea id="remark" type="text" class="form-control" placeholder="Remark" ng-model="remark" data-ng-required="true" rows="3" required></textarea>
        </div>
        <input style="margin-top: 10px;padding:7px; width:130px" class="btn btn-success" type="submit" value="Submit" ng-click="form.$valid && SaveQuestion()"/>
       </form>  
       </div>
    </div>
    
    <div class="panel panel-default">
            <div class="panel-heading">
                Registered Question Sets
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <form style="float:right;" class="form-inline">
                <div class="row align-items-center">
                    <div class="col text-right">
                        <div class="input-group">
                            <div style="padding-right: 25px; padding-top: 5px; padding-bottom: 5px;" class="row">
                            <input style="float: right;" id="search" class="form-control" type="text" ng-model="search" placeholder="&#128269; Search" type="search"/>
                            </div>
                        </div>
                    </div>
                  </div>
            </form>
            <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                    <thead>
                        <tr>
                         <td>Question Id</td>
                         <td>Question Set Id</td>
                         <td>Question</td>
                         <td>Choice A</td>
                         <td>Choice B</td>
                         <td>Choice C</td>
                         <td>Choice D</td>
                         <td>Choice E</td>
                         <td>Answer</td>
                         <td>Remark</td>
                         <td>Creator User Name</td>
                         <td>Action</td>
                   </tr>
                 </thead>
                 <tbody>
                     <tr ng-repeat="row in questions | filter: { question: search} ">
                         <td>{{row.id}}</td>
                         <td>{{row.questionSetId}}</td>
                         <td>{{row.question}}</td>
                         <td>{{row.choice_a}}</td>
                         <td>{{row.choice_b}}</td>
                         <td>{{row.choice_c}}</td>
                         <td>{{row.choice_d}}</td>
                         <td>{{row.choice_e}}</td>
                         <td>{{row.answer}}</td>
                         <td>{{row.remark}}</td>
                         <td>{{row.creatorUsername}}</td>
                         <td style="white-space: nowrap">
                            <a class="btn btn-danger" type="button" name="delete" ng-click="deleteQuestion(row.id)"><i class="fa fa-trash"></i></a>
                         </td>
                     </tr>
                 </tbody>
            </table>
          </div> 
    </div>
</div>

  <?php
                }
            ?>

<script src="dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $("#set").select2();
    $("#answer").select2();
    $('.js-example-basic-single').select2();
});