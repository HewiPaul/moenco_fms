<?php
include("../services/auth.php");
?>

<link rel="stylesheet" href="dist/css/select2.css">

 <?php
     if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
   ?>

<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
          Create New Assignment
    </div>
<!-- /.panel-heading -->
<div class="panel-body">
        <form enctype="multipart/form-data" role="form" name="form" ng-submit="SaveLicenseAssignmnet()"> 
        <div class="form-group">
              <label class="control-label" for="inputError">User</label>
              <select style="width:100%" id="user" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <option value="" selected disabled hidden>Choose here</option>
                    <option value="{{user.user_name}}" ng-repeat="user in users">{{user.full_name}}</option>
              </select>
        </div>
        <div class="form-group">
          <label class="control-label" for="inputError">Work Station</label>
          <input id="workStation" type="text" class="form-control" placeholder="Work Station" required/>
        </div>
        <div class="form-group">
          <label class="control-label" for="inputError">Operating System</label>
          <input id="os" type="text" class="form-control" placeholder="Operating System" required/>
        </div>
        <div class="form-group">
              <label class="control-label" for="inputError">License Type</label>
              <select style="width:100%" id="type" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <option value="" selected disabled hidden>Choose here</option>
                    <option value="CAT2 TGEMS">CAT2 TGEMS</option>
              </select>
        </div>
        <div class="form-group">
              <label class="control-label" for="inputError">License_issued</label>
              <select style="width:100%" id="license_issued" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <option value="" selected disabled hidden>Choose here</option>
                    <option value="{{license}}" ng-repeat="license in licenses">{{license.access_id}}</option>
              </select>
        </div>
        <div class="form-group">
              <label class="control-label" for="inputError">VPN Type</label>
              <select style="width:100%" id="vpn" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <option value="" selected disabled hidden>Choose here</option>
                    <option value="BIG-IP Edge Client">BIG-IP Edge Client</option>
              </select>
        </div>
              <div style="padding-top: 5px" class="form-group">
                                              <label> Location</label>                                           
			   <select style="width:100%" lass="js-example-basic-single form-control" id="location"
                              style="width:100%; color:black" data-required>
                              <option value="Addis Ababa" selected>Addis Ababa</option>
															<option value="Adama">Adama</option>
															<option value="Bahirdar 1">Bahirdar 1</option>
															<option value="Bahirdar 2">Bahirdar 2</option>
															<option value="Diredawa">Diredawa</option>
                          </select>
                </div>
        <input style="margin: 5px; width: 140px" class="btn btn-success" value="Submit Assignmnet" type="submit"/>
        </form>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
          Assignmnents
    </div>
<!-- /.panel-heading -->
<div class="panel-body">
<div class="col-md-12" class="row">
        <div class="table-responsive">
                      <!--<div style="float:right; padding:10px" class="text-right col-md-4">
                            <input ng-model="search" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>-->
        <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                <thead>
                    <tr>
                     <td>Case Id</td>
                     <td>User Name</td>
                     <td>Workstation</td>
                     <td>OS</td>
			   <td>License Type</td>
                     <td>License Issued</td>
                     <td>Status</td>
                     <td>Description</td>
                     <td>Start Date</td>
                     <td>Due Date</td>
                     <td>VPN Type</td>
                     <td>Location</td>
                     <td>Edit</td>
                     <td>Delete</td>
               </tr>
			    <tr>
                     <td><input class="form-control" placeholder="Case Id" ng-model="caseid" /></td>
                     <td><input class="form-control" placeholder="User Name" ng-model="username" /></td>
                     <td><input class="form-control" placeholder="Workstation" ng-model="workstation" /></td>
                     <td><input class="form-control" placeholder="OS" ng-model="OS" /></td>
                     <td><input class="form-control" placeholder="License Type" ng-model="licenseType" /></td>
                     <td><input class="form-control" placeholder="License Issued" ng-model="licenseIssued" /></td>
                     <td><input class="form-control" placeholder="Status" ng-model="status" /></td>
                     <td><input class="form-control" placeholder="Description" ng-model="description" /></td>
                     <td><input class="form-control" placeholder="Start Date" ng-model="StartDate" /></td>
                     <td><input class="form-control" placeholder="Due Date" ng-model="DueDate" /></td>
                     <td><input class="form-control" placeholder="VPN Type" ng-model="vapnType" /></td>
                     <td><input class="form-control" placeholder="Location" ng-model="location" /></td>
                     <td style="white-space: nowrap">
                      <button class="btn btn-sm" data-title="Edit" data-toggle="modal" data-target="#editUser" ng-click="FetchUser(row.Id)" disabled><i class="fa fa-edit"></i></button>
                     </td>
                      <td style="white-space: nowrap">
                        <button class="btn btn-sm" type="button" name="reset" ng-click="deleteLicenseAssignment(row.case_id)" disabled><i class="fa fa-trash"></i></button>
                     </td>
               </tr>
             </thead>
             <tbody>
                 <tr dir-paginate="row in assignments| filter: { case_id: caseid} | itemsPerPage:10" ng-model="search" pagination-id="row">
                     <td>{{row.case_id}}</td>
                     <td>{{row.username}}</td>
                     <td>{{row.WorkStation}}</td>
                     <td>{{row.OS}}</td>
                     <td>{{row.Cert_Type}}</td>
                     <td>{{row.Cer_Issue}}</td>
                     <td>{{row.Status}}</td>
                     <td>{{row.description}}</td>
                     <td>{{row.start_date}}</td>
                     <td>{{row.due_date}}</td>
                     <td>{{row.VPN_Type}}</td>
                     <td>{{row.Location}}</td>
                     <td style="white-space: nowrap">
                     <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editLicenseAssignment" ng-click="FetchLicenseAssignment(row.case_id)"><i class="fa fa-edit"></i></button></a>
                     </td>
                      <td style="white-space: nowrap">
                        <a class="btn btn-danger btn-sm" type="button" name="reset" ng-click="deleteLicenseAssignment(row.case_id)"><i class="fa fa-trash"></i></a>
                     </td>
                 </tr>
             </tbody>
        </table>
</div>
<div class="row" style="text-align:center">
 <dir-pagination-controls pagination-id="row" max-size="10" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
</div>
     </div>
     </div>
</div>
</div>



<div class="modal fade" id="editLicenseAssignment" tabindex="-1" role="dialog" aria-labelledby="editLicenseAssignment" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit License Assignment Data.</h4>
    </div>
              <div class="text-center text-muted mb-4">
                <small>License Assignment Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
              <input id="_case_id" type="hidden" ng-model="_case_id">
              <div class="form-group">
              <label class="control-label" for="inputError">User</label>
              <select style="width:100%" id="_user" ng-model="_user" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <!--<option value="" selected disabled hidden>Choose here</option>-->
                    <option value="{{user.user_name}}" ng-repeat="user in users">{{user.full_name}}</option>
              </select>
        </div>
        <div class="form-group">
          <label class="control-label" for="inputError">Work Station</label>
          <input id="_workStation" ng-model="_workStation" type="text" class="form-control" placeholder="Work Station" required/>
        </div>
        <div class="form-group">
          <label class="control-label" for="inputError">Operating System</label>
          <input id="_os" ng-model="_os" type="text" class="form-control" placeholder="Operating System" required/>
        </div>
        <div class="form-group">
              <label class="control-label" for="inputError">License Type</label>
              <select style="width:100%" id="_type" ng-model="_type" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <!--<option value="" selected disabled hidden>Choose here</option>-->
                    <option value="CAT2 TGEMS">CAT2 TGEMS</option>
              </select>
        </div>
        <div class="form-group">
              <label class="control-label" for="inputError">License_issued</label>
              <select style="width:100%" id="_license_issued" ng-model="_license_issued" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <option value="" selected disabled hidden>Choose here</option>-->
                    <option value="{{license}}" ng-repeat="license in licenses">{{license.access_id}}</option>
              </select>
        </div>
        <div class="form-group">
              <label class="control-label" for="inputError">VPN Type</label>
              <select style="width:100%" id="_vpn" ng-model="_vpn" class="form-control" name="state" class="js-example-basic-single form-control" required>
                    <!--<option value="" selected disabled hidden>Choose here</option>-->
                    <option value="BIG-IP Edge Client">BIG-IP Edge Client</option>
              </select>
        </div>
              <div style="padding-top: 5px" class="form-group">
                  <label> Location</label>                                           
			   <select style="width:100%" lass="js-example-basic-single form-control" id="_location" ng-model="_location"
                              style="width:100%; color:black" data-required>
                              <option value="Addis Ababa">Addis Ababa</option>
					<option value="Adama">Adama</option>
					<option value="Bahirdar 1">Bahirdar 1</option>
					<option value="Bahirdar 2">Bahirdar 2</option>
					<option value="Diredawa">Diredawa</option>
                          </select>
                </div>
              </form>
          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateLicenseAssignmnet()"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

  <?php
     }
  ?>

<script src="dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $("#user").select2();
    $('#location').select2();
    $('#type').select2();
    $('#license_issued').select2();
    $('#vpn').select2();
    $("#_user").select2();
    $('#_location').select2();
    $('#_type').select2();
    $('#_license_issued').select2();
    $('#_vpn').select2();
});