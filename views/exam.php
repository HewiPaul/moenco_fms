<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<link href="./portal/exam/style.css" rel="stylesheet" id="bootstrap-css">
<link href="./portal/exam/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="./bower_components/font-awesome/css/font-awesome.min.css">
<script src="./portal/exam/bootstrap.min.js"></script>
<script src="./portal/exam/jquery.min.js"></script>
<script src="./portal/exam/script.js"></script>
<style>
#examFrame::-webkit-scrollbar {
  width: 1em;
}
 
#examFrame::-webkit-scrollbar-track {
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
}
 
#examFrame::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
}
</style>
<!------ Include the above in your HEAD tag ---------->
<body>
<div id="examFrame" style="padding-top: 30px; overflow-y: scroll; height: 80vh; margin: 15px;" class="container-fluid">
    <div style="padding-top:5px; padding-bottom:5px" class="row col-md-4">
    <input style="margin-left: 10px;" type="text" class="form-control" id="candidateId" name="uniqueId" value="<?php echo $_SESSION['candidateId']; ?>" placeholder="Your ID" readonly>
    </div>
    <div style="padding-top:5px; padding-bottom:5px" class="row col-md-4">
    <input style="margin-left: 10px;" type="text" class="form-control" id="fullnameOnExam" name="fullname" value="<?php echo $_SESSION['candidateName']; ?>" placeholder="Your Full Name" readonly>
    </div>
    <div style="padding-top:5px; padding-bottom:5px" class="row col-md-4">
    <input style="margin-left: 10px;" type="text" class="form-control" id="examSessionExam" name="examSession" value="<?php echo $_SESSION['sessionName']; ?>" placeholder="Exam Session" readonly>
    </div>
  <form ng-submit="SubmitAnswers()">
  <div class="col-md-12">
      <div style="margin:20px auto;" class="modal-content col-md-12" ng-repeat="quez in questions">
         <div class="modal-header">
            <p><span class="label label-warning" id="qid">{{$index + 1}}</span><span style="padding:10px">{{quez.question}}<span></p>
        </div>
        <div class="modal-body">
            <!-- <div class="col-xs-3 col-xs-offset-5">
              <div id="loadbar" style="display: none;">
                  <div class="blockG" id="rotateG_01"></div>
                  <div class="blockG" id="rotateG_02"></div>
                  <div class="blockG" id="rotateG_03"></div>
                  <div class="blockG" id="rotateG_04"></div>
                  <div class="blockG" id="rotateG_05"></div>
                  <div class="blockG" id="rotateG_06"></div>
                  <div class="blockG" id="rotateG_07"></div>
                  <div class="blockG" id="rotateG_08"></div>
              </div>
          </div>-->

          <div class="quiz" id="quiz" data-toggle="buttons">
           <label class="element-animation1 btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon fa fa-angle-right"></i></span> <input type="radio" name="q_answer{{quez.id}}" value="A" required>A. {{quez.choice_a}}</label>
           <label class="element-animation2 btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon fa fa-angle-right"></i></span> <input type="radio" name="q_answer{{quez.id}}" value="B" required>B. {{quez.choice_b}}</label>
           <label class="element-animation3 btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon fa fa-angle-right"></i></span> <input type="radio" name="q_answer{{quez.id}}" value="C" required>C. {{quez.choice_c}}</label>
           <label class="element-animation4 btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon fa fa-angle-right"></i></span> <input type="radio" name="q_answer{{quez.id}}" value="D" required>D. {{quez.choice_d}}</label>
           <label class="element-animation4 btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon fa fa-angle-right"></i></span> <input type="radio" name="q_answer{{quez.id}}" value="E" required>E. {{quez.choice_e}}</label>
          </div>
   </div>
   <div class="row col-md-12">
   <div class="form-group col-md-6">
    <!--<a href="#/" class="btn btn-success"><i class="fa fa-save"></i> Save Progress</a>-->
    </div>
   </div>
   <div class="modal-footer text-muted">
    <span id="answer"></span>
</div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        <button style="padding: 10px; width:100%" type="submit" class="btn btn-warning"><i class="fa fa-check-circle"></i> Submit Answers</button>
    </div>
    <div class="form-group col-md-6">
        <button id="showResult" style="padding: 10px; padding-right:0px; float:right; width:100%" type="button" ng-click="showResult()" class="btn btn-success" disabled><i class="fa fa-file"></i> Show Result</button>
    </div>
</div>

</div>
</form>
</div>
<script>
    $( document ).ready(function() {
        document.getElementById("login").style.visibility = "hidden";
        document.getElementById("takeexam").style.visibility = "hidden";
    });
</script>
</body>
</html>