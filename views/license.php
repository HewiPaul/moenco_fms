<?php
include("../services/auth.php");
?>

            <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'FileAdministrator')){
            ?>

<div class="col-lg-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            New License
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
         <form enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form" ng-submit="SaveLicense()">
         <div class="form-group">
                <label class="control-label" for="inputError">Access Id</label>
          <input id="accessId" type="text" class="form-control" placeholder="Access Id" ng-model="accessId" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Start Date</label>
          <input id="startDate" type="date" class="form-control" placeholder="Start Date" ng-model="startDate" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Due Date</label>
          <input id="dueDate" type="date" class="form-control" placeholder="Due Date" ng-model="dueDate">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Status</label>
                <select id="status" class="js-example-basic-single form-control"
                      name="status" class="form-control" style="width:100%">
                      <option value="1">Assigned</option>
                      <option value="0" selected>Free</option>
                </select>
        </div>
       <div class="form-group">
                <label class="control-label" for="inputError">Priority</label>
          <input id="priority" type="number" class="form-control" placeholder="Priority" ng-model="priority" data-ng-required="true">
        </div>
       <div class="form-group">
                <label class="control-label" for="inputError">Description</label>
          <textarea id="description" type="text" class="form-control" placeholder="Description" ng-model="description"></textarea>
        </div>

        <input style="margin-top: 10px;padding:7px; width:130px" class="btn btn-success" type="submit" value="Submit"/>
       </form>  
    </div>
                   </div>
    
    <div class="panel panel-default">
            <div class="panel-heading">
                Registered Licenses
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <form style="float:right;" class="form-inline">
                <div class="row align-items-center">
                    <div class="col text-right">
                        <div class="input-group">
                            <div style="padding-right: 25px; padding-top: 5px; padding-bottom: 5px;" class="row">
                            <input style="float: right;" id="search" class="form-control" type="text" ng-model="search" placeholder="&#128269; Search" type="search"/>
                            </div>
                        </div>
                    </div>
                  </div>
            </form>
            <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                    <thead>
                        <tr>
                         <td>Access Id</td>
                         <td>Start Date</td>
                         <td>Due Date</td>
                         <td>Satus</td>
                         <td>Priority</td>
                         <td>Description</td>
                         <td>Creation Date</td>
                         <td>Action</td>
                   </tr>
                 </thead>
                 <tbody>
                     <tr dir-paginate="row in licenses | filter: { access_id: search} |itemsPerPage:5" pagination-id="row">
                         <td>{{row.access_id}}</td>
                         <td>{{row.start_date}}</td>
                         <td>{{row.due_date}}</td>
                         <td>{{row.status}}</td>
                         <td>{{row.priority}}</td>
                         <td>{{row.description}}</td>
                         <td>{{row.created_at}}</td>
                         <td style="white-space: nowrap">
                            <a class="btn btn-danger" type="button" name="delete" ng-click="deleteLicense(row.Id)"><i class="fa fa-trash"></i></a>
                            <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editLicense" ng-click="FetchLicense(row.Id)"><i class="fa fa-edit"></i></button></a> 
                        </td>
                     </tr>
                 </tbody>
            </table>
          </div> 
          <div style="text-align:center" class="row">
          <dir-pagination-controls pagination-id="row" max-size="5" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
          </div>
    </div>
</div>



  <div class="modal fade" id="editLicense" tabindex="-1" role="dialog" aria-labelledby="editLicense" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit License Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit License Data</small>
              </div>
              <form enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form">
         <div class="form-group">
                <label class="control-label" for="inputError">Access Id</label>
          <input id="_accessId" type="text" class="form-control" placeholder="Access Id" ng-model="_accessId" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Start Date</label>
          <input id="_startDate" type="text" class="form-control" placeholder="Start Date" ng-model="_startDate" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Due Date</label>
          <input id="_dueDate" type="text" class="form-control" placeholder="Due Date" ng-model="_dueDate" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Status</label>
                <select id="_status" class="js-example-basic-single form-control"
                      name="status" class="form-control" style="width:100%"
                      required>
                      <option value="1">Assigned</option>
                      <option value="0" selected>Free</option>
                </select>
        </div>
       <div class="form-group">
                <label class="control-label" for="inputError">Priority</label>
          <input id="_priority" type="text" class="form-control" placeholder="Priority" ng-model="_priority" data-ng-required="true">
        </div>
       <div class="form-group">
                <label class="control-label" for="inputError">Description</label>
          <textarea id="_description" type="text" class="form-control" placeholder="Description" ng-model="_description" data-ng-required="true"></textarea>
        </div>
       </form>  

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="updateLicense(_id)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>


 <?php
                }
            ?>