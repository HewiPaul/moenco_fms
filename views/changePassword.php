<?php
include("../services/auth.php");
?>

<div class="col-lg-12">
    
    <div class="panel panel-default">
        <div class="panel-heading">
              Change Password
        </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
            <form enctype="multipart/form-data" role="form" name="form"> 
            <div class="form-group">
              <label class="control-label" for="inputError">Username</label>
              <input type="text" id="uname" class="form-control" value="<?php echo $_SESSION['fsm_username']; ?>" disabled/>
              </div>
            <div class="form-group">
                    <label class="control-label" for="inputError">Old Password</label>
                  <input id="oldpass" type="password" class="form-control" placeholder="Old Password" ng-model="oldpassword" required>
                </div>
                <div class="form-group">
                    <label class="control-label" for="inputError">New Password</label>
                  <input id="newpass" type="password" class="form-control" placeholder="NewPassword" ng-model="newPassword" required>
                </div> 
                <div class="form-group">
                        <label class="control-label" for="inputError">Confirm New Password</label>
                      <input id="Cnewpass" type="password" class="form-control" placeholder="Confirm New Password" ng-model="CnewPassword" ng-pattern="newPassword" required />
                       <span ng-show="newPassword != CnewPassword">Your passwords must match.</span>
                </div>
            <input style="margin: 5px; width: 140px" class="btn btn-success" value="Change Password" type="submit" ng-click="form.$valid && changePassword()"/>
            </form>
    </div>
</div>
</div>