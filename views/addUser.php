<?php
include("../services/auth.php");
?>

<link rel="stylesheet" href="dist/css/select2.css">

 <?php
     if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
   ?>

<div class="col-lg-12">
<div class="panel panel-default">
    <div class="panel-heading">
          Create New User
    </div>
<!-- /.panel-heading -->
<div class="panel-body">
        <form enctype="multipart/form-data" role="form" name="form" ng-submit="Adduser()"> 
        <div class="form-group">
          <label class="control-label" for="inputError">Full Name</label>
          <input id="fullname" type="text" class="form-control" placeholder="Full Name" required/>
        </div>
        <div class="form-group">
          <label class="control-label" for="inputError">User Name</label>
          <input id="username" type="text" class="form-control" placeholder="User Name" required/>
        </div>
              <div style="padding-top: 5px" class="form-group">
                                              <label> Role</label>                                           
											    <select multiple class="js-example-basic-single form-control" id="role"
                                                            style="width:100%; color:black" name="states[]" multiple="multiple" data-required>
                                                            <option value="Guest" selected>Guest</option>
															<option value="SuperAdministrator">Super Administrator</option>
															<option value="RMAdministrator">RM Administrator</option>
															<option value="FileAdministrator">File Administrator</option>
															<option value="ExamAdministrator">Exam Administrator</option>
															<option value="RMUser">RM User</option>
															<option value="FileUser">File User</option>
                                                      </select>
                </div>
        <input style="margin: 5px; width: 140px" class="btn btn-success" value="Create User" type="submit"/>
        </form>
</div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
          Users
    </div>
<!-- /.panel-heading -->
<div class="panel-body">
                      <!--<div style="float:right; padding:10px" class="text-right col-md-4">
                            <input ng-model="search" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>-->
        <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                <thead>
                    <tr>
                     <td>Id</td>
                     <td>Full Name</td>
                     <td>User Name</td>
                     <td>Role</td>
					 <td>Status</td>
                     <td>Edit</td>
                     <td>Delete</td>
               </tr>
			    <tr>
                     <td><input class="form-control" placeholder="Id" ng-model="_Id" /></td>
                     <td><input class="form-control" placeholder="Full Name" ng-model="_fullname" /></td>
                     <td><input class="form-control" placeholder="User Name" ng-model="_username" /></td>
                     <td><input class="form-control" placeholder="Role" ng-model="_role" /></td>
					 <td><input class="form-control" placeholder="Staus" ng-model="_status" /></td>
                    <td style="white-space: nowrap">
                      <button class="btn btn-sm" data-title="Edit" data-toggle="modal" data-target="#editUser" ng-click="FetchUser(row.Id)" disabled><i class="fa fa-edit"></i></button>
                     </td>
                      <td style="white-space: nowrap">
                        <button class="btn btn-sm" type="button" name="reset" ng-click="deleteUser(row.Id)" disabled><i class="fa fa-trash"></i></button>
                     </td>
               </tr>
             </thead>
             <tbody>
                 <tr dir-paginate="row in users| filter: { Id: _Id} | filter: { full_name: _fullname} | filter: { user_name: _username} | filter: { role: _role} | filter: { status: _status} | itemsPerPage:10" ng-model="search" pagination-id="row">
                     <td>{{row.Id}}</td>
                     <td>{{row.full_name}}</td>
                     <td>{{row.user_name}}</td>
                     <td>{{row.role}}</td>
					 <td ng-if="row.last_active_time != null && row.last_active_time >= liveDateTime"><i class="fa fa-circle text-success"></i> {{row.status = 'Online'}}</td>
					 <td ng-if="row.last_active_time == null || liveDateTime == null || row.last_active_time < liveDateTime"><i class="fa fa-circle text-danger"></i> {{row.status = 'Offline'}}</td>
                     <!--<td>{{row.isAdmin == 1 ? 'True' : 'False'}}</td>-->
                     <td style="white-space: nowrap">
                     <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editUser" ng-click="FetchUser(row.Id)"><i class="fa fa-edit"></i></button></a>
                     </td>
                      <td style="white-space: nowrap">
                        <a class="btn btn-danger btn-sm" type="button" name="reset" ng-click="deleteUser(row.Id)"><i class="fa fa-trash"></i></a>
                     </td>
                 </tr>
             </tbody>
        </table>
</div>
<div class="row" style="text-align:center">
 <dir-pagination-controls pagination-id="row" max-size="10" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
</div>
</div>
</div>



<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="editUser" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit User Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>User Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
              <div class="form-group">
                    <input id="fullname_edit" class="form-control" placeholder="Full Name" type="text" ng-model="fullname" required>         
                </div>
                <div class="form-group">
                    <input id="id_edit" type="hidden" ng-model="id"/>
                    <input id="username_edit" class="form-control" placeholder="User Name" type="text" ng-model="username" required>         
                </div>
                <div class="form-group"> 
											   <select multiple class="js-example-basic-single form-control" id="role_edit"
                                                            style="width:100%; color:black" multiple="multiple" data-required>
                                                            <option value="Guest">Guest</option>
															<option value="SuperAdministrator">Super Administrator</option>
															<option value="RMAdministrator">RM Administrator</option>
															<option value="FileAdministrator">File Administrator</option>
															<option value="ExamAdministrator">Exam Administrator</option>
															<option value="RMUser">RM User</option>
															<option value="FileUser">File User</option>
                                                      </select>
                </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="updateUser()"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

  <?php
     }
  ?>

<script src="dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $("#role").select2();
    $('#role_edit').select2();
});