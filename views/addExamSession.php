<?php
include("../services/auth.php");
?>
<style>
input[type=time]::-webkit-datetime-edit-ampm-field {
  display: none;
}
</style>
<link rel="stylesheet" href="dist/css/select2.css">
            <?php
                if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'ExamAdministrator') !== false){
            ?>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Create New Exam Session
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
         <form enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form">
         <div class="form-group">
         <input id="userName" style="visibility:hidden; display:none" value="<?php echo $_SESSION['fsm_username']; ?>" />
                <label class="control-label" for="inputError">Exam Session Name</label>
          <input id="sessionName" type="text" class="form-control" placeholder="Exam Session Name" ng-model="sessionName" data-ng-required="true">
        </div>
        <div class="form-group">
                <label class="control-label" for="inputError">Exam Session Description</label>
          <textarea id="description" type="text" class="form-control" placeholder="Description" ng-model="description" data-ng-required="true" rows="3" required></textarea>
        </div>
         <div class="form-group">
                <input id="userName" style="visibility:hidden; display:none" value="<?php echo $_SESSION['fsm_username']; ?>" />
                <label class="control-label" for="inputError">Question Set</label>
                <select id="Quezset" class="js-example-basic-single form-control"
                      name="Quezset" class="form-control" ng-model="Quezset" style="width:100%"
                      required>
                      <option value="" selected disabled hidden>Choose here</option>
                      <option value="{{set.id}}" ng-repeat="set in sets">{{set.questionSet}}</option>
                </select>
           </div>
           <div class="form-group">
                <label class="control-label" for="inputError">Time Allowed</label>
          <input id="time" type="time"  class="form-control" placeholder="Allowed Time" ng-model="time">
         </div>
         <div class="form-group">
                <label class="control-label" for="inputError">Number Of Questions</label>
          <input id="quizlimit" type="number"  class="form-control" placeholder="Number Of Questions" ng-model="quizlimit" required>
         </div>
        <input style="margin-top: 10px;padding:7px; width:130px" class="btn btn-success" type="submit" value="Submit" ng-click="form.$valid && SaveExamSession()"/>
       </form>  
       </div>
    </div>
    
    <div class="panel panel-default">
            <div class="panel-heading">
                Registered Exam Sessions
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                    <thead>
                        <tr>
                         <td>Id</td>
                         <td>Question Set Id</td>
                         <td>Exam Session Name</td>
                         <td>Exam Session Description</td>
                         <td>Time Allowed</td>
                         <td>Exam Entrance Key</td>
                         <td>Questions Limit</td>
                         <td>Status</td>
                         <td>Created By</td>
                         <td>Action</td>
                   </tr>
                 </thead>
                 <tbody>
                     <tr ng-repeat="row in examSessions">
                         <td>{{row.id}}</td>
                         <td>{{row.questionSetId}}</td>
                         <td>{{row.sessionName}}</td>
                         <td>{{row.sessionDesc}}</td>
                         <td>{{row.timeAllowed}}</td>
                         <td>{{row.examKey}}</td>
                         <td>{{row.questionLimit}}</td>
                         <td>{{row.status}}</td>
                         <td>{{row.creatorUser}}</td>
                         <td style="white-space: nowrap">
                            <a class="btn btn-danger" type="button" name="delete" ng-click="deleteExamSession(row.id)"><i class="fa fa-trash"></i></a>
                         </td>
                     </tr>
                 </tbody>
            </table>
          </div> 
    </div>
</div>

  <?php
                }
            ?>

<script src="dist/js/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $("#Quezset").select2();
    $('.js-example-basic-single').select2();  
    document.getElementById("time").defaultValue = "02:00";
});