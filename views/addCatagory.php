<?php
include("../services/auth.php");
?>

            <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'FileAdministrator')){
            ?>

<div class="col-lg-12">

    <div class="panel panel-default">
        <div class="panel-heading">
            New Catagory
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
         <form enctype="multipart/form-data" style="padding: 5px; padding-top: 10px" role="form" name="form">
         <div class="form-group">
                <label class="control-label" for="inputError">Catagory Name</label>
          <input id="catName" type="text" class="form-control" placeholder="Catagory Name" ng-model="catName" data-ng-required="true">
        </div>
        <input style="margin-top: 10px;padding:7px; width:130px" class="btn btn-success" type="submit" value="Submit" ng-click="form.$valid && SaveCatagory()"/>
       </form>  
       </div>
    </div>
    
    <div class="panel panel-default">
            <div class="panel-heading">
                Registered Catagory
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                    <thead>
                        <tr>
                         <td>Catagory Id</td>
                         <td>Catagory Name</td>
                         <td>Action</td>
                   </tr>
                 </thead>
                 <tbody>
                     <tr ng-repeat="row in catagories">
                         <td>{{row.id}}</td>
                         <td>{{row.name}}</td>
                         <td style="white-space: nowrap">
                            <a class="btn btn-danger" type="button" name="delete" ng-click="deleteCatagory(row.id)"><i class="fa fa-trash"></i></a>
                         </td>
                     </tr>
                 </tbody>
            </table>
          </div> 
    </div>
</div>
 <?php
                }
            ?>