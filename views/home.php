<?php
include("../services/auth.php");
?>
  <section class="content-header">
    <h1>
      Dashboard
    </h1>
    <ul class="breadcrumb">
      <li><a href="#/"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a> Dashboard </a></li>
    </ul>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row col-md-12">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
            <h3>{{catagoryCount}}</h3>
          <p>Total Catagories</p>
        </div>
        <div class="icon">
          <i class="fa fa-list"></i>
        </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
        <a href="#/addCatagory" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        <?php
                }
        ?>
      </div>
    </div>
    <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-primary"> 
            <div class="inner">
                <h3>{{fileCount}}</h3>
              <p>Total Shared Files</p>
            </div>
            <div class="icon">
              <i class="fa fa-file"></i> 
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/sharedFiles" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            <?php
                }
            ?>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
                <h3>{{userCount}}</h3>
              <p>Total Users</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            <?php
                }
            ?>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
             <h3 style="font-size: 37px;">{{today}}</h3>
              <p>Today</p>
            </div>
            <div class="icon">
              <i class="fa fa-calendar"></i>
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/" class="small-box-footer"><i class="fa fa-arrow-circle-right"> </i> Current Date</a>
            <?php
                }
            ?>
          </div>
        </div>
        <!-- ./col -->
        </div>

          <div class="row col-md-12">
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
             <h3 style="font-size: 37px;">{{manualCount}}</h3>
              <p>Repair Manuals</p>
            </div>
            <div class="icon">
              <i class="fa fa-cog"></i>
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/repairManual" class="small-box-footer"><i class="fa fa-arrow-circle-right"> </i> More Info</a>
            <?php
                }
            ?>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-purple">
            <div class="inner">
             <h3 style="font-size: 37px;">{{sessionCount}}</h3>
              <p>Exam Sessions</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/newExamSession" class="small-box-footer"><i class="fa fa-arrow-circle-right"> </i> More Info</a>
            <?php
                }
            ?>
          </div>
        </div>


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray">
            <div class="inner">
             <h3 style="font-size: 37px;">{{questionCount}}</h3>
              <p>Exam Questions</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"></i>
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/questionPerSessionReport" class="small-box-footer"><i class="fa fa-arrow-circle-right"> </i> More Info</a>
            <?php
                }
            ?>
          </div>
        </div>
		
		 <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-olive">
            <div class="inner">
             <h3 style="font-size: 37px; color:white">{{OnlineUserCount}}</h3>
              <p style="color:white">Online Users</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
            <a href="#/users" class="small-box-footer"><i class="fa fa-arrow-circle-right"> </i> More Info</a>
            <?php
                }
            ?>
          </div>
        </div>


        </div>
        <!-- ./col -->
        </section>