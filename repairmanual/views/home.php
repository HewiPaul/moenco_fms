<?php
include("../../services/auth.php");
?>
<link rel="stylesheet" href="./repairmanual/assets/css/select2.css">
<link href="./repairmanual/assets/css/bootstrap.min.css" rel="stylesheet">
<style>
  #viewer{
    position:absolute;
    height:1%;
    width:1%;
    visibility:hidden;
  }
  tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
   <?php
     if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'RMAdministrator') !== false|| strpos($_SESSION['role'], 'RMUser') !== false){
   ?>
<div id="home" class="content">
     <div style="padding-top: 5px;" class="content">
		<h4 style="font-weight:bolder; padding-left:5px">Repair Manuals</h4>
      <div style="background-color: rgb(231, 231, 255); border-radius: 15px; padding: 10px; padding-top:15px" class="col-md-12" style="padding-top: 15px;" class="row">
      <form style="padding:10px"  role="form" ng-submit="filter(destination, brand, model, generalcode, productiondate, fullcode)">
        <div style="padding-top:10px" class="form-group col-md-2">
            <select style="width:100%" id="destination" name="destination" ng-model="destination" ng-change="filterUsingDestination(model, destination, brand, generalcode, productiondate, fullcode)" class="form-control js-example-basic-single" >
              <option value="" selected disabled hidden>Select Destination</option>
              <option value="">All</option> 
              <option ng-repeat="des in destinations" value="{{des}}">{{des.name}}</option>
            </select>
        </div>
        <div style="padding-top:10px" class="form-group col-md-2">
            <select style="width:100%" name="brand" id="brand" class="form-control js-example-basic-single" ng-model="brand" ng-change="filterUsingBrand(model, destination, brand, generalcode, productiondate, fullcode)">
              <option value="" selected disabled hidden>Select Brand</option>
              <option ng-repeat="br in brands" value="{{br}}">{{br.name}}</option>
            </select>  
        </div>
        <div style="padding-top:10px" class="form-group col-md-2">
            <select style="width:100%" name="model" id="model" class="form-control js-example-basic-single" placeholder="model" ng-model="model" ng-change="filterUsingModel(model, destination, brand, generalcode, productiondate, fullcode)">
              <option value="" selected disabled hidden>Select Model</option>
              <option ng-repeat="mod in models" value="{{mod}}">{{mod.name}}</option>
            </select>  
        </div>
        <div style="padding-top:10px" class="form-group col-md-2">
            <select style="width:100%" name="generalcode" id="generalcode" class="form-control js-example-basic-single" ng-model="generalcode" ng-change="filterUsingGN(model, destination, brand, generalcode, productiondate, fullcode)">
              <option value="" selected disabled hidden>Select General Code</option>
              <option ng-repeat="gcd in generalcodes" value="{{gcd}}">{{gcd.code}}</option>
            </select>  
        </div>
        <div style="padding-top:10px" class="form-group col-md-2">
            <input id="productiondate" class="form-control" placeholder="Production Date" type="text" ng-model="productiondate" ng-change="fetchByDate(model, destination, brand, generalcode, productiondate, fullcode)">
        </div>
        <div style="padding-top:10px" class="form-group col-md-2">
            <select style="width:100%" id="fullcode" class="form-control js-example-basic-single" placeholder="Full Code" ng-model="fullcode" ng-change="fetchByFullCode(model, destination, brand, generalcode, productiondate, fullcode)">
              <option value="" selected disabled hidden>Select Full Code</option>            
              <option ng-repeat="fcd in fullcodes" value="{{fcd}}">{{fcd.code}}</option>
            </select>  
        </div>
        <!--<div style="text-align: center;" class="row col-md-6">
          <button style="width:120px;" class="btn btn-primary pull-left" type="submit">Search &raquo;</button>
        </div>-->
      </form >
      </div>
      <div style="padding-top:10px" class="col-md-12" class="row">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover table-responsive" id="example" style="width:100%">
            <thead class="bg-primary">
              <tr>
                      <th scope="col">Production Date</th>
                      <th scope="col">Brand</th>
					  <th scope="col">Model</th>
                      <th scope="col">General Code</th>
					  <th scope="col">Full Code</th>
                      <th scope="col">Link</th>
              </tr>
            </thead >
            <tbody>
                  <tr ng-repeat="row in manuals|filter:search" ng-model="search" >
                   <td><h6 >{{row.production_date}}</h6></td>
				          <td><h6 >{{row.brand}}</h6></td>
                   <td><h6 >{{row.model}}</h6></td>
                   <td><h6 >{{row.general_code}}</h6></td>
				            <td><h6 >{{row.full_code}}</h6></td>
                   <td><h6 ><a href="{{row.link}}" target="_blank">View Manual</a></h6 ></td>
              </tr>
            </tbody>
			<tfoot>
             <tr>
                      <th scope="col">Production Date</th>
                      <th scope="col">Model</th>
                      <th scope="col">Brand</th>
                      <th scope="col">General Code</th>
					            <th scope="col">Full Code</th>
                      <th scope="col">Link</th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!--<dir-pagination-controls pagination-id="row" max-size="6" direction-links="true" boundary-links="true">
          </dir-pagination-controls>-->
      </div>
      </div>

        </div>
        </div>
        <!--<iframe id="viewer" frameborder="0" allowfullscreen></iframe>-->

  <?php
     }
  ?>
        <script src="./repairmanual/scripts/select2.min.js"></script>
        <script>
          // In your Javascript (external .js resource or <script> tag)
          $(document).ready(function () {
                $('.js-example-basic-single').select2();
          });
    </script>
  <!-- Page content -->