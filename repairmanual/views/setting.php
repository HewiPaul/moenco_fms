<?php
include("../../services/auth.php");
?>
        <div class="row col-md-12 content">
          <div class="col-md-12 content">
      <div class="card bg-secondary shadow border-0">
        <div class="card-body px-lg-5 py-lg-5">
          <div class="text-center text-muted mb-4">
            <small>Account Setting</small>
          </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
                <form enctype="multipart/form-data" role="form" name="form" ng-submit="changePassword()"> 
                <div class="form-group">
                  <label class="control-label" for="inputError">Username</label>
                  <input id="uname" type="text" class="form-control" value="<?php echo $_SESSION['fsm_username']; ?>" readonly="true"/>
                </div>
                    <div class="form-group">
                        <label class="control-label" for="inputError">New Password</label>
                      <input id="newpass" type="password" class="form-control" placeholder="NewPassword" ng-model="newPassword" required>
                    </div> 
                    <div class="form-group">
                            <label class="control-label" for="inputError">Confirm New Password</label>
                          <input id="Cnewpass" type="password" class="form-control" placeholder="Confirm New Password" ng-model="CnewPassword" ng-pattern="newPassword" required />
                           <span style="font-size:12px; color:red" ng-show="newPassword != CnewPassword">Your confirmation password must match.</span>
                    </div>
                <input style="margin: 5px; width: 200px" class="btn btn-success" value="Change Password" type="submit"/>
                </form>
        </div>
    </div>
  </div>
</div>