<?php
include("../../services/auth.php");
?>
 <?php
     if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'RMAdministrator') !== false){
   ?>
<link rel="stylesheet" href="./repairmanual/assets/css/select2.css">
<link href="./repairmanual/assets/css/bootstrap.min.css" rel="stylesheet">


<div class="row col-md-12">
    <div class="row col-md-12 content">
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item active">
      <a class="nav-link" id="home-tab" data-toggle="tab" data-target="#destination" role="tab" aria-controls="home" aria-selected="true"><h5 style="padding-left: 10px; padding-right: 10px;">Destination</h5></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" data-target="#brand" role="tab" aria-controls="contact" aria-selected="false"><h5 style="padding-left: 10px; padding-right: 10px;">Brand</h5></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="profile-tab" data-toggle="tab" data-target="#model" role="tab" aria-controls="profile" aria-selected="false"><h5 style="padding-left: 10px; padding-right: 10px;">Model</h5></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" data-target="#generalCode" role="tab" aria-controls="contact" aria-selected="false" style="padding-left: 10px; padding-right: 10px;"><h5>General Code</h5></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" data-target="#fullCode" role="tab" aria-controls="contact" aria-selected="false"><h5 style="padding-left: 10px; padding-right: 10px;">Full Code</h5></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" data-target="#manual" role="tab" aria-controls="contact" aria-selected="false"><h5 style="padding-left: 10px; padding-right: 10px;">Manual</h5></a>
    </li>
  </ul>


  <div class="tab-content" id="myTabContent">
    <div style="padding: 10px;" class="tab-pane active" id="destination" role="tabpanel" aria-labelledby="home-tab"> 
      <div class="row content">
        <div class="row col-lg-12 col-md-12">
          <div class="card bg-secondary shadow border-0">
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>Register New Destination</small>
              </div>
              <form role="form" ng-submit="addDestination()">
                <div class="form-group">
                    <input id="name" class="form-control" placeholder="Name" type="text" ng-model="name" required>
                </div>
                <div class="form-group">
                    <input id="description" class="form-control" placeholder="Description" type="text" ng-model="description" required>
                </div>
                <div style="text-align: center;" class="row col-xs-6 col-md-6">
                  <button class="btn btn-primary pull-left" type="submit">Save &raquo;</button>
                </div>
              </form>
              <br><br>
                  

              <div style="margin-top: 10px;" class="col-xl-12 mb-5 mb-xl-0">
                <div style="padding: 5px" class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div style="float:right" class="text-right col-md-4">
                            <input ng-model="search1" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-striped table-bordered table-hover table-responsiv">
                      <thead class="bg-primary">
                        <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr dir-paginate="row in destinations|filter:search1|itemsPerPage:5" ng-model="search" pagination-id="row1" >
                             <td>{{row.Id}}</td>
                             <td>{{row.name}}</td>
                             <td>{{row.description}}</td>
                             <td><button class="btn btn-sm btn-danger" ng-click="deleteDescription(row.Id)"><i class="fa fa-trash"></i></button>
                             <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editDestination" ng-click="FetchDescription(row.Id)"><i class="fa fa-edit"></i></button></a>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
				  <dir-pagination-controls pagination-id="row1" max-size="5" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
              </div>
            </div>
          </div>
        </div>
       </div>
    </div>
    
    
    
    <div class="tab-pane fade" id="model" role="tabpanel" aria-labelledby="profile-tab">        
    <div class="row content">
        <div class="row col-lg-12 col-md-12">
              <div class="text-center text-muted mb-4">
                <small>Register New Model</small>
              </div>
              <form role="form" ng-submit="addModel()">
              <div class="form-group">
              <select style="width:100%" name="brandId" id="brandId" class="form-control js-example-basic-single" ng-model="brandId" data-ng-required="true">
                <option value="" selected disabled hidden>Select Brand</option>
                <option ng-repeat="br in brands" value="{{br.Id}}">{{br.name}}</option>
              </select>  
          </div>
                <div class="form-group">
                    <input id="name2" class="form-control" placeholder="Name" type="text" ng-model="name2" required>
                </div>
                <div class="form-group">
                    <input id="description2" class="form-control" placeholder="Description" type="text" ng-model="description2" required>
                </div>
                <div style="text-align: center;" class="row col-md-6">
                  <button class="btn btn-primary pull-left" type="submit">Save &raquo;</button>
                </div>
              </form>
              <br><br>
                  

              <div style="margin-top: 10px;" class="col-xl-12 mb-5 mb-xl-0">
                <div style="padding: 5px" class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div style="float:right" class="col-md-4 text-right">
                            <input ng-model="search2" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-striped table-bordered table-hover table-responsiv">
                      <thead class="bg-primary">
                        <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr dir-paginate="row in models|filter:search2|itemsPerPage:5" ng-model="search" pagination-id="row2" >
                             <td>{{row.Id}}</td>
                             <td>{{row.name}}</td>
                             <td>{{row.description}}</td>
                             <td><button class="btn btn-sm btn-danger" ng-click="deleteModel(row.Id)"><i class="fa fa-trash"></i></button> 
                             <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editModel" ng-click="FetchModel(row.Id)"><i class="fa fa-edit"></i></button></a>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
				  		<dir-pagination-controls pagination-id="row2" max-size="5" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
                </div>
              </div>
            </div>
          </div>
    </div>
    
    


    <div class="tab-pane fade" id="brand" role="tabpanel" aria-labelledby="contact-tab">


            
      <div class="row content col-md-12">
        <div class=" row content col-md-12">
              <div class="text-center text-muted mb-4">
                <small>Register New Brand</small>
              </div>
              <form role="form" ng-submit="addBrand()">
              <div class="form-group">
              <select style="width:100%" id="destinationId" name="destinationId" ng-model="destinationId" class="form-control js-example-basic-single" class="form-control" data-ng-required="true">
                <option value="" selected disabled hidden>Select Destination</option>
                <option ng-repeat="des in destinations" value="{{des.Id}}">{{des.name}}</option>
              </select>
          </div>
                <div class="form-group">
                    <input id="name3" class="form-control" placeholder="Name" type="text" ng-model="name3" required>
                </div>
                <div class="form-group">
                    <input id="description3" class="form-control" placeholder="Description" type="text" ng-model="description3" required>
                </div>
                <div style="text-align: center;" class="row col-md-6">
                  <button class="btn btn-primary pull-left" type="submit">Save &raquo;</button>
                </div>
              </form>
                <br>
                  

              <div style="margin-top: 10px;" class="col-xl-12 mb-5 mb-xl-0">
                <div style="padding: 5px" class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div style="float:right" class="col-md-4 text-right">
                            <input ng-model="search3" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-striped table-bordered table-hover table-responsiv">
                      <thead class="bg-primary">
                        <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr dir-paginate="row in brands|filter:search3|itemsPerPage:5" ng-model="search" pagination-id="row3" >
                             <td>{{row.Id}}</td>
                             <td>{{row.name}}</td>
                             <td>{{row.description}}</td>
                             <td><button class="btn btn-sm btn-danger" ng-click="deleteBrand(row.Id)"><i class="fa fa-trash"></i></button>
                             <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editBrand" ng-click="FetchBrand(row.Id)"><i class="fa fa-edit"></i></button></a>
                             </td>
                            </tr>
                      </tbody>
                    </table>
                  </div>
                </div>			
                  <dir-pagination-controls pagination-id="row3" max-size="5" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
              </div>
            </div>
         </div>
    </div>



    <div class="tab-pane fade" id="generalCode" role="tabpanel" aria-labelledby="contact-tab">

    <div class="row content col-md-12">
        <div class=" row content col-md-12">
              <div class="text-center text-muted mb-4">
                <small>Register New General Code</small>
              </div>
              <form role="form" ng-submit="addGeneralCode()">
              <div class="form-group">
              <select style="width:100%" name="modelId" id="modelId" class="form-control js-example-basic-single" placeholder="model" ng-model="modelId" data-ng-required="true">
                <option value="" selected disabled hidden>Select Model</option>
                <option ng-repeat="mod in models" value="{{mod.Id}}">{{mod.name}}</option>
              </select>  
              </div>
                <div class="form-group">
                    <input id="name4" class="form-control" placeholder="Name" type="text" ng-model="name4" required>
                </div>
                <div class="form-group">
                    <input id="description4" class="form-control" placeholder="Description" type="text" ng-model="description4" required>
                  </div>
                <div style="text-align: center;" class="row col-md-6">
                  <button class="btn btn-primary pull-left" type="submit">Save &raquo;</button>
                </div>
              </form>
              <br>
                  

              <div style="margin-top: 10px; text-align:center" class="col-md-12">
                <div style="padding: 5px" class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div style="float:right" class="col-md-4 row text-right">
                            <input ng-model="search4" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-striped table-bordered table-hover table-responsive">
                      <thead class="bg-primary">
                        <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Code</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr dir-paginate="row in generalcodes|filter:search4|itemsPerPage:5" ng-model="search" pagination-id="row4" >
                             <td>{{row.Id}}</td>
                             <td>{{row.code}}</td>
                             <td>{{row.description}}</td>
                             <td><button class="btn btn-sm btn-danger" ng-click="deleteGeneralCode(row.Id)"><i class="fa fa-trash"></i></button>
                             <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editGeneralCode" ng-click="FetchGeneralCode(row.Id)"><i class="fa fa-edit"></i></button></a>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>				
                  <dir-pagination-controls pagination-id="row4" max-size="5" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
              </div>
            </div>
    </div>
    </div>



    <div class="tab-pane fade" id="fullCode" role="tabpanel" aria-labelledby="contact-tab">

    <div class="row content col-md-12">
        <div class=" row content col-md-12">
              <div class="text-center text-muted mb-4">
                <small>Register New Full Code</small>
              </div>
              <form role="form" ng-submit="addFullCode()">
              <div class="form-group">
              <select style="width:100%" name="generalcodeId" id="generalcodeId" class="form-control js-example-basic-single" ng-model="generalcodeId" data-ng-required="true">
                <option value="" selected disabled hidden>Select General Code</option>
                <option ng-repeat="gcd in generalcodes" value="{{gcd.Id}}">{{gcd.code}}</option>
              </select>  
          </div>
                <div class="form-group">
                    <input id="name5" class="form-control" placeholder="Name" type="text" ng-model="name5" required>
                </div>
                <div class="form-group">
                    <input id="description5" class="form-control" placeholder="Description" type="text" ng-model="description5" required>
                </div>
                <div style="text-align: center;" class="row col-md-6">
                  <button class="btn btn-primary pull-left" type="submit">Save &raquo;</button>
                </div>
              </form>
              <br><br>
                  

              <div style="margin-top: 10px; text-align:center" class="col-md-12">
                <div style="padding: 5px" class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div style="float:right" class="col-md-4 row text-right">
                            <input ng-model="search5" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table table-striped table-bordered table-hover table-responsive">
                      <thead class="bg-primary">
                        <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Code</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                            <tr dir-paginate="row in fullcodes|filter:search5|itemsPerPage:5" ng-model="search" pagination-id="row5" >
                             <td>{{row.Id}}</td>
                             <td>{{row.code}}</td>
                             <td>{{row.description}}</td>
                             <td><button class="btn btn-sm btn-danger" ng-click="deleteFullCode(row.Id)"><i class="fa fa-trash"></i></button>
                             <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#editFullCode" ng-click="FetchFullCode(row.Id)"><i class="fa fa-edit"></i></button></a>
                            </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>			
                  <dir-pagination-controls pagination-id="row5" max-size="5" direction-links="true" boundary-links="true">
                    </dir-pagination-controls>
              </div>
            </div>
          </div>
    </div>



    <div class="tab-pane fade" id="manual" role="tabpanel" aria-labelledby="contact-tab">
    <div class="row content col-md-12">
        <form  role="form" ng-submit="saveManual()">
          <div class="form-group">
              <select style="width:100%" id="destination2" name="destination2" ng-model="destination2" class="js-example-basic-single form-control" ng-change="fetchForBrand(destination2)" data-ng-required="true">
                <option value="" selected disabled hidden>Select Destination</option>
                <option ng-repeat="des in destinations" value="{{des}}">{{des.name}}</option>
              </select>
          </div>
          <div class="form-group">
              <select style="width:100%" name="brand2" id="brand2" class="form-control js-example-basic-single" ng-model="brand2" ng-change="fetchForModel(brand2)" data-ng-required="true">
                <option value="" selected disabled hidden>Select Brand</option>
                <option ng-repeat="br in brands_" value="{{br}}">{{br.name}}</option>
              </select>  
          </div>
          <div class="form-group">
              <select style="width:100%" name="model2" id="model2" class="form-control js-example-basic-single" placeholder="model" ng-model="model2" ng-change="fetchForGeneralCode(model2)" data-ng-required="true">
                <option value="" selected disabled hidden>Select Model</option>
                <option ng-repeat="mod in models_" value="{{mod}}">{{mod.name}}</option>
              </select>  
          </div>
          <div class="form-group">
              <select style="width:100%" name="generalcode2" id="generalcode2" class="form-control js-example-basic-single" ng-model="generalcode2" ng-change="fetchForFullCode(generalcode2)" data-ng-required="true">
                <option value="" selected disabled hidden>Select General Code</option>
                <option ng-repeat="gcd in generalcodes_" value="{{gcd}}">{{gcd.code}}</option>
              </select>  
          </div>
          <div class="form-group">
              <input id="productiondate2" class="form-control" placeholder="Production Date" type="text" ng-model="productiondate2" data-ng-required="true">
            </div>
          <div class="form-group">
              <select style="width:100%" id="fullcode2" class="form-control js-example-basic-single" placeholder="Full Code" ng-model="fullcode2" data-ng-required="false">
                <option value="" selected disabled hidden>Select Full Code</option>             
                <option ng-repeat="fcd in fullcodes_" value="{{fcd}}">{{fcd.code}}</option>
              </select> 
          </div>
          <div class="form-group">
              <select style="width:100%" id="language2" class="form-control js-example-basic-single" placeholder="Language" ng-model="language2" data-ng-required="true">
                <option value="" selected disabled hidden>Select Language</option>             
                <option value="AM">AM</option>
                <option value="EN" selected>EN</option>
                <option value="RUS">RUS</option>
                <option value="RUS">FR</option>
                <option value="RUS">POR</option>
              </select>  
          </div>
          <div class="form-group">
              <input id="link2" class="form-control" ng-model="link" placeholder="Link" data-ng-required="true"/> 
          </div>
          <div style="text-align: center;" class="row col-md-6">
            <button style="width:120px" class="btn btn-primary pull-left" type="submit">Save &raquo;</button>
          </div>
        </form >
        </div>
  
        <div style="margin-top: 10px; text-align:center" class="col-md-12">
                <div style="padding: 5px" class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div style="float:right" class="col-md-4 row text-right">
                            <input ng-model="search5" type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-responsive">
              <thead class="bg-primary">
                <tr>
                        <th scope="col">Production Date</th>
                        <th scope="col">Brand</th>
                        <th scope="col">Model</th>
                        <th scope="col">General Code</th>
                        <th scope="col">Link</th>
                        <th scope="col">Action</th>
                </tr>
              </thead >
              <tbody>
                    <tr dir-paginate="row in manuals|filter:search|itemsPerPage:6" ng-model="search" pagination-id="rowManual" >
                      <td>{{row.production_date}}</td>
                      <td>{{row.brand}}</td>
                     <td>{{row.model}}</td>
                     <td>{{row.general_code}}</td>
                     <td><a href="{{row.link}}" target="_blank">Link</a></td>
                     <td style="min-width:100px">
                      <button class="btn btn-sm btn-danger" ng-click="deleteManual(row.Id)"><i class="fa fa-trash"></i></button>
                      <a data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-sm" data-title="Edit" data-toggle="modal" data-target="#edit" ng-click="FetchManual(row.Id)"><i class="fa fa-edit"></i></button></a>
                      </td>
                </tr>
              </tbody>
            </table>
            </div>
           </div>
          <dir-pagination-controls pagination-id="rowManual" max-size="6" direction-links="true" boundary-links="true">
            </dir-pagination-controls>
        </div>
        </div>
    </div>

  </div>
  </div>
  </div>



<!--Modals-->

  <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Manual Data.</h4>
      </div>
          <div class="modal-body">
          <div class="form-group">
              <input id="id_" type="hidden" ng-model="_id"/>
              <select id="destination_" name="destination_" ng-model="_destination" class="form-control" class="form-control" data-ng-required="true">
                <option value="" selected disabled hidden>Select Destination</option>
                <option ng-repeat="des in destinations" value="{{des.name}}">{{des.name}}</option>
              </select>
          </div>
          <div class="form-group">
              <select name="brand_" id="brand_" class="form-control" ng-model="_brand" data-ng-required="true">
                <option value="" selected disabled hidden>Select Brand</option>
                <option ng-repeat="br in brands" value="{{br.name}}">{{br.name}}</option>
              </select>  
          </div>
          <div class="form-group">
              <select name="model_" id="model_" class="form-control" placeholder="model" ng-model="_model" data-ng-required="true">
                <option value="" selected disabled hidden>Select Model</option>
                <option ng-repeat="mod in models" value="{{mod.name}}">{{mod.name}}</option>
              </select>  
          </div>
          <div class="form-group">
              <select name="generalcode_" id="generalcode_" class="form-control" ng-model="_generalcode" data-ng-required="true">
                <option value="" selected disabled hidden>Select General Code</option>
                <option ng-repeat="gcd in generalcodes" value="{{gcd.code}}">{{gcd.code}}</option>
              </select>  
          </div>
          <div class="form-group">
              <input id="productiondate_" class="form-control" placeholder="Production Date" type="text" ng-model="productiondate2" data-ng-required="true">
          </div>
          <div class="form-group">
              <select id="fullcode_" class="form-control" placeholder="Full Code" ng-model="_fullcode" data-ng-required="true">
                <option value="" selected disabled hidden>Select Full Code</option>             
                <option ng-repeat="fcd in fullcodes" value="{{fcd.code}}">{{fcd.code}}</option>
              </select> 
          </div>
          <div class="form-group">
              <select id="language_" class="form-control" placeholder="Language" ng-model="_language" data-ng-required="true">
                <option value="" selected disabled hidden>Select Language</option>             
                <option value="AM">AM</option>
                <option value="EN" selected>EN</option>
                <option value="RUS">RUS</option>
                <option value="RUS">FR</option>
                <option value="RUS">POR</option>
              </select>  
          </div>
          <div class="form-group">
              <input id="link_" class="form-control" ng-model="_link" placeholder="Link" data-ng-required="true"/> 
          </div>
          </div>
          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateManual(_id)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>




    

  <div class="modal fade" id="editDestination" tabindex="-1" role="dialog" aria-labelledby="editDestination" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Destination Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit Destination Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
                <div class="form-group">
                    <input id="id_2" type="hidden" ng-model="_id2"/>
                    <input id="_name2" class="form-control" placeholder="Name" type="text" ng-model="_name2" required>
            
                </div>
                <div class="form-group">
                    <input id="_description2" class="form-control" placeholder="Description" type="text" ng-model="_description2" required>
                  </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateDestination(_id2)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
    


    <div class="modal fade" id="editModel" tabindex="-1" role="dialog" aria-labelledby="editModel" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Model Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit Model Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
                <div class="form-group">
                    <input id="id_3" type="hidden" ng-model="_id3"/>
                    <input id="_name3" class="form-control" placeholder="Name" type="text" ng-model="_name3" required>
              
                </div>
                <div class="form-group">
                    <input id="_description3" class="form-control" placeholder="Description" type="text" ng-model="_description3" required>
                  </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateModel(_id3)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>


    
    <div class="modal fade" id="editBrand" tabindex="-1" role="dialog" aria-labelledby="editBrand" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Brand Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit Brand Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
                <div class="form-group">
                    <input id="id_4" type="hidden" ng-model="_id4"/>
                    <input id="_name4" class="form-control" placeholder="Name" type="text" ng-model="_name4" required>
               
                </div>
                <div class="form-group">
                    <input id="_description4" class="form-control" placeholder="Description" type="text" ng-model="_description4" required>              
                </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateBrand(_id4)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>



    <div class="modal fade" id="editGeneralCode" tabindex="-1" role="dialog" aria-labelledby="editGeneralCode" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit General Code Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit General Code Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
                <div class="form-group">
                    <input id="id_5" type="hidden" ng-model="_id5"/>
                    <input id="_name5" class="form-control" placeholder="Code" type="text" ng-model="_name5" required>

                </div>
                <div class="form-group">
                    <input id="_description5" class="form-control" placeholder="Description" type="text" ng-model="_description5" required>
          
                </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateGeneralCode(_id5)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>



    <div class="modal fade" id="editFullCode" tabindex="-1" role="dialog" aria-labelledby="editFullCode" aria-hidden="true">
      <div class="modal-dialog">
    <div class="modal-content">
          <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
        <h4 class="modal-title custom_align" id="Heading">Edit Full Code Data.</h4>
    </div>

              <div class="text-center text-muted mb-4">
                <small>Edit Full Code Data</small>
              </div>
              <form style="padding-left:10px; padding-right:10px" role="form">
                <div class="form-group">
                    <input id="id_6" type="hidden" ng-model="_id6"/>
                    <input id="_name6" class="form-control" placeholder="Code" type="text" ng-model="_name6" required>
             
                </div>
                <div class="form-group">
                    <input id="_description6" class="form-control" placeholder="Description" type="text" ng-model="_description6" required>
          
                </div>
              </form>

          <div class="modal-footer ">
        <button type="button" class="btn btn-warning btn-lg" style="width: 100%;" ng-click="UpdateFullCode(_id6)"><span class="glyphicon glyphicon-ok-sign"></span> Update Data </button>
      </div>
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
    <script src="./repairmanual/scripts/select2.min.js"></script>
        <script>
          // In your Javascript (external .js resource or <script> tag)
          $(document).ready(function () {
                $('.js-example-basic-single').select2();
          });
    </script>
  <?php
     }
  ?>
  