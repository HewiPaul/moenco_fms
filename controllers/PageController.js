/// <reference path="../dist/js/angular.js" />

var app = angular.module("FSManagment", ["ngRoute", "angularUtils.directives.dirPagination"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "views/home.php",
                controller: "mainController"
            })
            .when("/repairManual", {
                templateUrl: "repairmanual/views/home.php",
                controller: "RepairManualController"
            })
            .when("/data_managment", {
                templateUrl: "repairmanual/views/data.php",
                 controller: "RMdataController"
            })
            .when("/user_managment", {
                templateUrl: "repairmanual/views/users.php",
                 controller: "RMusersController"
            })
            .when("/RMsetting", {
                templateUrl: "repairmanual/views/setting.php",
                 controller: "RMusersController"
            })
            .when("/addCatagory", {
                templateUrl: "views/addCatagory.php",
                controller: "mainController"
            })
            .when("/fileManager", {
                templateUrl: "views/fileManager.php",
                controller: "fileManagerController"
            })
            .when("/users", {
                templateUrl: "views/addUser.php",
                controller: "userController"
            })
            .when("/changePassword", {
                templateUrl: "views/changePassword.php",
                controller: "userController"
            })
            .when("/sharedFiles", {
                templateUrl: "views/sharedFiles.php",
                controller: "sharedFilesController"
            })
            .when("/viewFile", {
                templateUrl: "views/pdfView.html",
                controller: "pdfViewerController"
            })
            .when("/questionSet", {
                templateUrl: "views/addQuestionSet.php",
                controller: "ExamController"
            })
            .when("/newQuestion", {
                templateUrl: "views/addQuestion.php",
                controller: "ExamController"
            })
            .when("/newExamSession", {
                templateUrl: "views/addExamSession.php",
                controller: "ExamController"
            })
            .when("/examReport" , {
                templateUrl: "views/examReport.htm",
                controller: "ExamController"
            })
            .when("/examReportPerapp", {
                templateUrl: "views/examReport2.htm",
                controller: "ExamController"
            })
            .when("/questionPerSessionReport", {
                templateUrl: "views/questionsPerExamSessionReport.htm",
                controller: "ExamController"
            })
            .when("/registerCandidate", {
                templateUrl: "views/candidateRegistrationBackoffice.htm",
                controller: "ExamController"
            })
            .when("/examSetting", {
                templateUrl: "views/examSetting.htm",
                controller: "ExamController"
            })
            .when("/licenses", {
                templateUrl: "views/license.php",
                controller: "TGMSController"
            })
            .when("/licenseAssignment", {
                templateUrl: "views/licenseAssignmnet.php",
                controller: "TGMSController"
            });
    });

var curDate = new Date();
var curDay = new Date().getDate();
var curDateMonth = new Date().getMonth() + 1; // we must have to add 1 to get the correcr month
var curDateYear = new Date().getFullYear();
var now = curDateYear + '-' + curDateMonth + '-' + curDay;

var fileUrl = "";
var fileName = "";
var pageSize = 0;
var base64String = "";

app.run(['$http', function ($http) {
            
}]);


app.controller('mainController', function ($scope, $http, $window) {

    $scope.today = now;
	
	update_last_Activity();	
	
	    var username = document.getElementById("username").innerHTML;
		var role = document.getElementById("_role").innerHTML;
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
		
		$http.post('services/countOnlineUsers.php', {
        tableName: 'users'
        }).then(function (response) {
        $scope.OnlineUserCount = response.data.OnlineUserCount;
        });
		
	    update_last_Activity();
	},5000);
	
	 	     function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			fetchusers();
		}); 
		};
    
	function fetchusers(){
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
	};

    $http.post('services/FetchData.php', {
        tableName: 'catagory'
        }).then(function (response) {
        //console.log(response.data);
        $scope.catagories = response.data;
    });

    $http.post('services/countFiles.php', {
        tableName: 'files'
        }).then(function (response) {
        console.log(response.data.fileCount);
        $scope.fileCount = response.data.fileCount;
    });

    $http.post('services/countCatagory.php', {
        tableName: 'catagory'
        }).then(function (response) {
        console.log(response.data.catagoryCount);
        $scope.catagoryCount = response.data.catagoryCount;
    });

    $http.post('services/countExamSessions.php', {
        tableName: 'examsession'
        }).then(function (response) {
        console.log(response.data.sessionCount);
        $scope.sessionCount = response.data.sessionCount;
    });

      $http.post('services/countExamQuestions.php', {
        tableName: 'questions'
        }).then(function (response) {
        console.log(response.data.questionCount);
        $scope.questionCount = response.data.questionCount;
    });


    $http.post('services/countManuals.php', {
        tableName: 'manual'
        }).then(function (response) {
        console.log(response.data.manualCount);
        $scope.manualCount = response.data.manualCount;
    });

    $http.post('services/countUsers.php', {
        tableName: 'users'
        }).then(function (response) {
        console.log(response.data.userCount);
        $scope.userCount = response.data.userCount;
    });
	
	$http.post('services/countOnlineUsers.php', {
        tableName: 'users'
        }).then(function (response) {
        console.log(response.data.OnlineUserCount);
        $scope.OnlineUserCount = response.data.OnlineUserCount;
    });

    function refereshCatagory(){
        $http.post('services/FetchData.php', {
            tableName: 'catagory'
            }).then(function (response) {
            //console.log(response.data);
            $scope.catagories = response.data;
        });
    };

    $scope.SaveCatagory = function () {
        $http({
            method: 'POST',
            url: "services/addCatagory.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("catName", document.getElementById("catName").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refereshCatagory();
            document.getElementById("catName").value = "";
            swal("Success", data, "success");
        });
		
		$http.post('services/writeLog.php', {
		log: "- username: " + username + " | role: " + role + " | Operation: Remove File Catagory | DateTime: " + new Date() + " \n\n"
        }).then(function (res) {
			   console.log(res.data);
		}); 
		
    };

    $scope.deleteCatagory = function(Id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this item!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $http.post('services/delete.php', {
                tableName: 'catagory',
                id: Id
                      }).then(function (response) {
                        refereshCatagory();
                        console.log(response.data);
                        swal("Success", "Catagory Deleted Successfully!" , "success");
                      });
					   $http.post('services/writeLog.php', {
						log: "- username: " + username + " | role: " + role + " | Operation: Remove File Catagory | DateTime: " + new Date() + " \n\n"
						}).then(function (res) {
							   console.log(res.data);
						}); 
            };
          });
    };

    $scope.refreshPage = function () {
        window.location.reload();
    };

});




app.controller('fileManagerController', function ($scope, $http, $window) {
	
		update_last_Activity();	
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
		
	    var username = document.getElementById("username").innerHTML;
		var role = document.getElementById("_role").innerHTML;
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	     function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			fetchusers();
		}); 
		};
    
	function fetchusers(){
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
	};

    $scope.pages = "1";

    $http.post('services/FetchData.php', {
        tableName: 'files'
        }).then(function (response) {
        //console.log(response.data);
        $scope.files_ = response.data;
    });

    $http.post('services/FetchData.php', {
        tableName: 'catagory'
        }).then(function (response) {
        //console.log(response.data);
        $scope.catagories_ = response.data;
    });

        function refereshFiles(){
        $http.post('services/FetchData.php', {
            tableName: 'files'
            }).then(function (response) {
            //console.log(response.data);
            $scope.files_ = response.data;
        });
    };

    $scope.SaveFile = function () {
        //$scope.file1 = $scope.files[0];
        $http({
            method: 'POST',
            url: "services/addFile.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("file1", $scope.file1 != null ? $scope.file1 : "");
                formData.append("name", $scope.name);
                formData.append("pages", $scope.pages);
                formData.append("catagory", $scope.catagory);
                formData.append("dir", $scope.dir);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refereshFiles();
            swal({
                title: "Success",
                text: data,
                icon: "success"
              });
        });
				$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Upload File | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.FetchDescription =  function (_id){
        $http.post('services/fetchSingleData.php', {tableName: 'files', id: _id})
        .then(function (res) {
           $scope.id = res.data[0].id;
           $scope.name2 = res.data[0].name;
           $scope.dir2 = res.data[0].directory.replace("files/", "");;
           $scope.pages2 = res.data[0].pages;
           $scope.catagory2 = res.data[0].catagory;
        });
    };
    
    $scope.UpdateFile = function (id) {
        $http({
            method: 'POST',
            url: "services/updateFile.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("file1", $scope.file1 != null ? $scope.file1 : "");
                formData.append("id", id);
                formData.append("name2", $scope.name2);
                formData.append("pages2", $scope.pages2);
                formData.append("catagory2", $scope.catagory2);
                formData.append("dir2", $scope.dir2);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refereshFiles();
            swal({
                title: "Success",
                text: "File Info. Updated Successfully!",
                icon: "success"
              });
              $('#edit').modal('hide');
        });
				$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update Uploaded File | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.deleteFile = function(Id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this item!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $http.post('services/delete.php', {
                tableName: 'files',
                id: Id
                      }).then(function (response) {
                        refereshFiles();
                        console.log(response.data);
                        swal("Success", "File Deleted Successfully!" , "success");
                      });
            };
          });
		  
		  $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Delete Uploaded File | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.uploadedFile = function (element) {

        //$scope.currentFile = element.files[0];

        var fReader = new FileReader();
		fReader.readAsDataURL(element.files[0]);
		fReader.onloadend = function(event){ 
        $scope.file1 =  event.target.result;
        console.log(event.target.result);     
        };

        //reader.onload = function (event) {
            //var output = document.getElementById('output');
            //output.src = URL.createObjectURL(element.files[0]);
    
            //$scope.file_source = event.target.result
            //$scope.$apply(function ($scope) {
            //});
        //}
        //reader.readAsDataURL(element.files[0]);
    //};
};
});


app.controller('userController', function ($scope, $http, $window) {
	    $scope.userStatus = "";
		
		update_last_Activity();	
		
	    var username = document.getElementById("username").innerHTML;
		var role = document.getElementById("_role").innerHTML;
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	    function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			//console.log(res.data);
		    fetchusers();
		}); 
		};

    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 

    function fetchusers() {
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
    };


    $scope.resetPwd = function (Id) {
        if (confirm('Are you sure to reset this user password ?')) {
        $http.post('services/resetPassword.php', {
            tableName: 'users',
            id: Id
        }).then(function (response) {
            alert("User Password Reseted Successfully!");
        });
    }
    };

    $scope.deleteUser = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/delete.php', {
                tableName: 'users',
                id: Id
                      }).then(function (response) {
                        fetchusers();
                        swal("Success", "User Deleted Successfully!" , "success");
                      });
            };
          });
		  		  $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Delete User | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

$scope.changePassword = function () {
    $http({
        method: 'POST',
        url: "services/changePassword.php",
        processData: false,
        transformRequest: function (data) {
            var formData = new FormData();
            formData.append("uname", document.getElementById("uname").value);
            formData.append("newpass", document.getElementById("newpass").value);
            return formData;
        },
        data: $scope.form,
        headers: {
            'Content-Type': undefined
        }
    }).success(function (data) {
        document.getElementById("oldpass").value = "";
        document.getElementById("newpass").value = "";
        document.getElementById("Cnewpass").value = "";
        swal("Success", data , "success");
        $window.location.href = '#/';
    });
			  $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Password Change | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
};

$scope.FetchUser =  function (_id){
    $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'users', id: _id})
    .then(function (res) {
       $scope.id = res.data[0].Id;
       $scope.username = res.data[0].user_name;
       $scope.fullname = res.data[0].full_name;
	   $("#role_edit").val(res.data[0].role.split(",")).trigger("change");
    });
};

$scope.Adduser = function(){
		var selectedRoles = String($("#role").val());
        var Rolearray = selectedRoles.split(' , ');
		
    $http.post('repairmanual/services/fetchsingleUser.php', {tableName: 'users', username: document.getElementById("username").value})
    .then(function (res) {
      if(res.data.length > 0){
        swal({
            title: "Notice",
            text: "This Username is Already Exist!",
            icon: "warning",
            dangerMode: true,
          });
      }
      else{
        $http({
            method: 'POST',
            url: "repairmanual/services/adduser.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("fullname", document.getElementById("fullname").value);
                formData.append("username", document.getElementById("username").value);
                formData.append("role", Rolearray.toString());
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            fetchusers();
            document.getElementById("fullname").value = "";
            document.getElementById("username").value = "";
            swal("Success", data , "success");
        });
				  $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Register User | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
      }
    });
};

$scope.updateUser = function(){
    var selectedRoles = String($("#role_edit").val());
    var Rolearray = selectedRoles.split(',');
    $http({
        method: 'POST',
        url: "services/updateUser.php",
        processData: false,
        transformRequest: function (data) {
            var formData = new FormData();
            formData.append("id", $scope.id);
            formData.append("fullname", document.getElementById("fullname_edit").value);
            formData.append("username", document.getElementById("username_edit").value);
            formData.append("role", Rolearray);
            return formData;
        },
        data: $scope.form,
        headers: {
            'Content-Type': undefined
        }
    }).success(function (data) {
        fetchusers();
        swal("Success", data , "success");
    });
			   $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update User | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
};

});


app.controller('sharedFilesController', function ($scope, $http, $window) {
	
		update_last_Activity();	
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	 	     function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			fetchusers();
		}); 
		};
    
	function fetchusers(){
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
	};
		
    
    $http.post('services/FetchData.php', {
        tableName: 'catagory'
        }).then(function (response) {
        //console.log(response.data);
        $scope.catas = response.data;
    });

    $http.post('services/FetchData.php', {
        tableName: 'files'
        }).then(function (response) {
        //console.log(response.data);
        $scope.files_ = response.data;
    });

    $scope.setFileValues = function(fileurl, filename, _pageSize, _base64String){
        fileName = filename;
        fileUrl = fileurl;
        pageSize = _pageSize;
        base64String = _base64String;
    };
});

app.controller('pdfViewerController', function ($scope, $http, $window) {
        $scope.fileName = fileName;
        $scope.fileUrl = fileUrl;
        $scope.pageSize = pageSize;
        $scope.base64File = base64String;
});

app.controller('ExamController', function ($scope, $http, $window) {

    var Id = 0;
	
		update_last_Activity();	
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	    function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
		}); 
		};
   
    $http.post('services/FetchData.php', {
        tableName: 'questionset'
        }).then(function (response) {
        //console.log(response.data);
        $scope.sets = response.data;
    });

    $http.post('services/FetchData.php', {
        tableName: 'questions'
        }).then(function (response) {
        //console.log(response.data);
        $scope.questions = response.data;
    });

    $http.post('services/FetchData.php', {
        tableName: 'examsession'
        }).then(function (response) {
        //console.log(response.data);
        $scope.examSessions = response.data;
    });

    $http.post('services/FetchData.php', {
        tableName: 'candidates'
        }).then(function (response) {
        //console.log(response.data);
        $scope.candidates = response.data;
    });

    $http.post('services/getExamReport.php', {
        tableName: 'table'
        }).then(function (response) {
        console.log(response.data);
        $scope.examReport = response.data;
    });
    
    $http.post('services/getExamReport2.php', {
        tableName: 'table'
        }).then(function (response) {
        console.log(response.data);
        $scope.examReport2 = response.data;
    });
    

    function refreshSet(){
        $http.post('services/FetchData.php', {
            tableName: 'questionset'
            }).then(function (response) {
            //console.log(response.data);
            $scope.sets = response.data;
        });
    };

    function refreshQuestion(){
        $http.post('services/FetchData.php', {
            tableName: 'questions'
            }).then(function (response) {
            //console.log(response.data);
            $scope.questions = response.data;
        });
    };

    function refershExamSession(){
        $http.post('services/FetchData.php', {
            tableName: 'examsession'
            }).then(function (response) {
            //console.log(response.data);
            $scope.examSessions = response.data;
        });
    };

    function refreshCandidates(){
        $http.post('services/FetchData.php', {
            tableName: 'candidates'
            }).then(function (response) {
            //console.log(response.data);
            $scope.candidates = response.data;
        });
    
    };

    $scope.calcTotal = function(filtered){
        var sum = 0;
        for(var i = 0 ; i<filtered.length ; i++){
           sum = sum + filtered[i].isRight * 1;
        }
        return sum;
   };

   $http.post('services/getLastApplicant.php', {tableName: 'candidates'})
   .then(function (res) {
      if(res.data != null && res.data.Id > 0)
           Id = (res.data.Id * 1) + 1;
       else
           Id = 1;
      //$scope.notice = res.data[0];
   });

   $scope.candidateSelected = function(_id){
    $http.post('services/fetchSingleData.php', {tableName: 'candidates', id: _id})
    .then(function (res) {
       $scope.fullname = res.data[0].fullname;
       $scope.uniqueId = res.data[0].uniqueId;
    });
   };

   $scope.ResetExam = function(){
    var uniqueId = $scope.uniqueId;
    var sessionId = document.getElementById("sessionId").value;
    $http.post('services/resetExam.php', {tableName: 'examrecord', id: uniqueId, sessionID: sessionId})
    .success(function (data) {
        swal("Success", data , "success");
    });
   };
   
    $scope.SaveQuestionSet = function () {
        $http({
            method: 'POST',
            url: "services/addQuestionSet.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("name", document.getElementById("setName").value);
                formData.append("desc", document.getElementById("setDesc").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            document.getElementById("setName").value = "";
            document.getElementById("setDesc").value = "";
            swal("Success", data , "success");
            refreshSet();
            //$window.location.reload();
        });
    };


    $scope.SaveQuestion = function () {
        $http({
            method: 'POST',
            url: "services/addQuestion.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("set", document.getElementById("set").value);
                formData.append("userName", document.getElementById("userName").value);
                formData.append("question", document.getElementById("question").value);
                formData.append("A", document.getElementById("A").value);
                formData.append("B", document.getElementById("B").value);
                formData.append("C", document.getElementById("C").value);
                formData.append("D", document.getElementById("D").value);
                formData.append("E", document.getElementById("E").value);
                formData.append("answer", document.getElementById("answer").value);
                formData.append("remark", document.getElementById("remark").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            swal("Success", data , "success");
            refreshQuestion();
            //$window.location.reload();
        });
    };
  
    $scope.SaveExamSession = function () {
        $http({
            method: 'POST',
            url: "services/addExamSession.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("sessionName", document.getElementById("sessionName").value);
                formData.append("description", document.getElementById("description").value);
                formData.append("Quezset", document.getElementById("Quezset").value);
                formData.append("time", document.getElementById("time").value);
                formData.append("userName", document.getElementById("userName").value);
                formData.append("quizlimit", document.getElementById("quizlimit").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            swal("Success", data , "success");
            refershExamSession();
            //$window.location.reload();
        });
    };

    $scope.SaveRegistration = function () {
        $http({
            method: 'POST',
            url: "services/candidateRegistration.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("uniqueId","APP-" + Id)
                formData.append("fullname", $scope.fname + ' ' + $scope.lname + ' ' + $scope.gname);
                formData.append("age", $scope.age);
                formData.append("gender", document.getElementById("gender").value);
                formData.append("email", $scope.email);
                formData.append("phone", $scope.phone);
                formData.append("session", document.getElementById("session").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshCandidates();
            swal({
                title: "Success",
                text: data + " Your Applicant ID is: APP-" + Id,
                icon: "success"
              });  
              $scope.fname = ""; 
              $scope.lname = ""; 
              $scope.gname = ""; 
              $scope.age = "";
              document.getElementById("gender").value = "";
              $scope.email = "";
              $scope.phone = "";
              document.getElementById("session").value = "";
        });
    };


    $scope.deleteSet = function(id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/delete.php', {
                tableName: 'questionset',
                id: id
                      }).then(function (response) {
                        refreshSet();
                        console.log(response.data);
                        swal("Success", "Question Set Deleted Successfully!" , "success");
                      });
            };
          });
    };

    $scope.deleteQuestion = function(id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/delete.php', {
                tableName: 'questions',
                id: id
                      }).then(function (response) {
                        refreshQuestion();
                        console.log(response.data);
                        swal("Success", "Question Deleted Successfully!" , "success");
                      });
            };
          });
    };

    $scope.deleteExamSession = function(id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/delete.php', {
                tableName: 'examsession',
                id: id
                      }).then(function (response) {
                        refershExamSession();
                        console.log(response.data);
                        swal("Success", "Exam Session Deleted Successfully!" , "success");
                      });
            };
          });
    };
    
});



app.controller('RepairManualController', function ($scope, $http, $window) {  

	update_last_Activity();	
	
		var username = document.getElementById("username").innerHTML;
		var role = document.getElementById("_role").innerHTML;
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	  function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			fetchusers();
		}); 
		};
    
	function fetchusers(){
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
	};
        
    $http.post('repairmanual/services/fetchdata.php', {tableName: 'manual'})
    .then(function (res) {
       $scope.manuals = res.data != null ? res.data : 0;
	   angular.element(document).ready(function() { 
	   			 // Setup - add a text input to each footer cell
				$('#example tfoot th').each( function () {
					var title = $(this).text();
					$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
				} );
			 
				// DataTable
				var table = $('#example').DataTable({
					initComplete: function () {
						// Apply the search
						this.api().columns().every( function () {
							var that = this;
			 
							$( 'input', this.footer() ).on( 'keyup change clear', function () {
								if ( that.search() !== this.value ) {
									that
										.search( this.value )
										.draw();
								}
							} );
						} );
					}
				});
	      }); 
    });

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'destination_tbl'})
    .then(function (res) {
       $scope.destinations = res.data != null ? res.data : 0;
    });

      $scope.showFile = function(path){
        alert("am here");
        document.getElementById("frame").src = path;
      };
    //Brand Filter Using Destination && Manual Data Filter

        $scope.filterUsingDestination = function(_model, _destination, _brand, _generalcode, _date, _fullcode){
            $scope.brand = null;
            $scope.model = null;
            $scope.models = null;
            $scope.generalcode = null;
            $scope.generalcodes = null;
            $scope.fullcode = null;
            $scope.fullcodes = null;
            $scope.productiondate = "";
            _model = null;
            _brand = null;
            _date = null;
            _generalcode = null;
            _fullcode = null;

            if(_destination != "" && _destination != null){
            var id = _destination != null && _destination != "" ? JSON.parse(_destination).Id : 0;
            var destinationName = _destination != null && _destination != "" ? JSON.parse(_destination).name : 0;
            var brandName = _brand != null && _brand != ""  ?  JSON.parse(_brand).name : 0;
            var modelName = _model != null && _model != "" ?  JSON.parse(_model).name : 0;
            var generalCode = _generalcode != null && _generalcode != ""  ? JSON.parse(_generalcode).code : 0;
            var fullCode = _fullcode != null && _fullcode != "" ? JSON.parse(_fullcode).code : 0;

           $http.post('repairmanual/services/fetchdataForBrand.php', {tableName: 'brand_tbl', destinationId: id})
           .then(function (res) {
              $scope.brands = res.data != null ? res.data : 0;
           });
           }
           else{
            $scope.brands = "";
           }

           $http.post('repairmanual/services/fetchfiltereddata.php', {tableName: 'manual', destination: destinationName != null ? destinationName : 0, model:modelName != null ? modelName : 0, brand: brandName != null ? brandName : 0, generalcode:generalCode != null ? generalCode : 0, date:_date != null ? _date : 0, fullcode:fullCode != null ? fullCode : 0})
           .then(function (res) {
           $scope.manuals = res.data != null ? res.data : null;
         });

        };

        //Model Filter Using Brand && Manual Data Filter

        $scope.filterUsingBrand = function(_model, _destination, _brand, _generalcode, _date, _fullcode){
            $scope.model = null;
            $scope.models = null;
            $scope.generalcode = null;
            $scope.productiondate = "";
            $scope.generalcodes = null;
            $scope.fullcode = null;
            $scope.fullcodes = null;
            _model = null;
            _generalcode = null;
            _fullcode = null;
            _date = null;

            if(_brand != "" && _brand != null){
                var brandId_ = _brand != null && _brand != "" ?  JSON.parse(_brand).Id : 0;
                var destinationName = _destination != null && _destination != "" ? JSON.parse(_destination).name : 0;
                var brandName = _brand != null && _brand != ""  ?  JSON.parse(_brand).name : 0;
                var modelName = _model != null && _model != "" ?  JSON.parse(_model).name : 0;
                var generalCode = _generalcode != null && _generalcode != ""  ? JSON.parse(_generalcode).code : 0;
                var fullCode = _fullcode != null && _fullcode != "" ? JSON.parse(_fullcode).code : 0;

               $http.post('repairmanual/services/fetchdataForModel.php', {tableName: 'model_tbl', brandId: brandId_})
               .then(function (res) {
                  $scope.models = res.data != null ? res.data : 0;
               });
               }
               else{
                $scope.models = "";
               }

               $http.post('repairmanual/services/fetchfiltereddata.php', {tableName: 'manual', destination: destinationName != null ? destinationName : 0, model:modelName != null ? modelName : 0, brand: brandName != null ? brandName : 0, generalcode:generalCode != null ? generalCode : 0, date:_date != null ? _date : 0, fullcode:fullCode != null ? fullCode : 0})
               .then(function (res) {
               $scope.manuals = res.data != null ? res.data : null;
             });
        };


        //General Code Filter Using Model && Manual Data Filter

        $scope.filterUsingModel = function(_model, _destination, _brand, _generalcode, _date, _fullcode){
            
            $scope.generalcode = null;
            $scope.generalcodes = null;
            $scope.fullcode = null;
            $scope.fullcodes = null;
            $scope.productiondate = "";
            _generalcode = null;
            _fullcode = null;
            _date = null;

            if(_model != "" && _model != null){
                var modelId_ = _model != null && _model != ""  ?  JSON.parse(_model).Id : 0;
                var destinationName = _destination != null && _destination != "" ? JSON.parse(_destination).name : 0;
                var brandName = _brand != null && _brand != ""  ?  JSON.parse(_brand).name : 0;
                var modelName = _model != null && _model != "" ?  JSON.parse(_model).name : 0;
                var generalCode = _generalcode != null && _generalcode != ""  ? JSON.parse(_generalcode).code : 0;
                var fullCode = _fullcode != null && _fullcode != "" ? JSON.parse(_fullcode).code : 0;

               $http.post('repairmanual/services/fetchdataForGenralCode.php', {tableName: 'generalcode_tbl', modelId: modelId_})
               .then(function (res) {
                  $scope.generalcodes = res.data != null ? res.data : 0;
               });
               }
               else{
                $scope.generalcodes = "";
               }

               $http.post('repairmanual/services/fetchfiltereddata.php', {tableName: 'manual', destination: destinationName != null ? destinationName : 0, model:modelName != null ? modelName : 0, brand: brandName != null ? brandName : 0, generalcode:generalCode != null ? generalCode : 0, date:_date != null ? _date : 0, fullcode:fullCode != null ? fullCode : 0})
               .then(function (res) {
               $scope.manuals = res.data != null ? res.data : null;
             });
        };

        //Full Code Filter Using Model && Manual Data Filter

        $scope.filterUsingGN = function(_model, _destination, _brand, _generalcode, _date, _fullcode){
            
            $scope.fullcode = null;
            $scope.fullcodes = null;
            $scope.productiondate = "";
            _fullcode = null;
            _date = null;

            if(_generalcode != "" && _generalcode != null){
                var generalCodeId_ = _generalcode != null && _generalcode != ""  ?  JSON.parse(_generalcode).Id : 0;
                var destinationName = _destination != null && _destination != "" ? JSON.parse(_destination).name : 0;
                var brandName = _brand != null && _brand != ""  ?  JSON.parse(_brand).name : 0;
                var modelName = _model != null && _model != "" ?  JSON.parse(_model).name : 0;
                var generalCode = _generalcode != null && _generalcode != ""  ? JSON.parse(_generalcode).code : 0;
                var fullCode = _fullcode != null && _fullcode != "" ? JSON.parse(_fullcode).code : 0;

               $http.post('repairmanual/services/fetchdataForFullCode.php', {tableName: 'fullcode_tbl', generalCodeId: generalCodeId_})
               .then(function (res) {
                  $scope.fullcodes = res.data != null ? res.data : 0;
               });
               }
               else{
                $scope.fullcodes = "";
               }

               $http.post('repairmanual/services/fetchfiltereddata.php', {tableName: 'manual', destination: destinationName != null ? destinationName : 0, model:modelName != null ? modelName : 0, brand: brandName != null ? brandName : 0, generalcode:generalCode != null ? generalCode : 0, date:_date != null ? _date : 0, fullcode:fullCode != null ? fullCode : 0})
               .then(function (res) {
               $scope.manuals = res.data != null ? res.data : null;
             });
        };


        $scope.fetchByDate = function(_model, _destination, _brand, _generalcode, _date, _fullcode){
                var fullCodeId_ = _fullcode != null && _fullcode != ""  ?  JSON.parse(_fullcode).Id : 0;
                var destinationName = _destination != null && _destination != "" ? JSON.parse(_destination).name : 0;
                var brandName = _brand != null && _brand != ""  ?  JSON.parse(_brand).name : 0;
                var modelName = _model != null && _model != "" ?  JSON.parse(_model).name : 0;
                var generalCode = _generalcode != null && _generalcode != ""  ? JSON.parse(_generalcode).code : 0;
                var fullCode = _fullcode != null && _fullcode != "" ? JSON.parse(_fullcode).code : 0;

               $http.post('repairmanual/services/fetchfiltereddata.php', {tableName: 'manual', destination: destinationName != null ? destinationName : 0, model:modelName != null ? modelName : 0, brand: brandName != null ? brandName : 0, generalcode:generalCode != null ? generalCode : 0, date:_date != null ? _date : 0, fullcode:fullCode != null ? fullCode : 0})
               .then(function (res) {
               $scope.manuals = res.data != null ? res.data : null;
             });
         };

         $scope.fetchByFullCode = function(_model, _destination, _brand, _generalcode, _date, _fullcode){
            var fullCodeId_ = _fullcode != null && _fullcode != ""  ?  JSON.parse(_fullcode).Id : 0;
            var destinationName = _destination != null && _destination != "" ? JSON.parse(_destination).name : 0;
            var brandName = _brand != null && _brand != ""  ?  JSON.parse(_brand).name : 0;
            var modelName = _model != null && _model != "" ?  JSON.parse(_model).name : 0;
            var generalCode = _generalcode != null && _generalcode != ""  ? JSON.parse(_generalcode).code : 0;
            var fullCode = _fullcode != null && _fullcode != "" ? JSON.parse(_fullcode).code : 0;

           $http.post('repairmanual/services/fetchfiltereddata.php', {tableName: 'manual', destination: destinationName != null ? destinationName : 0, model:modelName != null ? modelName : 0, brand: brandName != null ? brandName : 0, generalcode:generalCode != null ? generalCode : 0, date:_date != null ? _date : 0, fullcode:fullCode != null ? fullCode : 0})
           .then(function (res) {
           $scope.manuals = res.data != null ? res.data : null;
         });
     };


        $scope.changePassword = function () {
            $http({
                method: 'POST',
                url: "repairmanual/services/changePassword.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("uname", document.getElementById("uname").value);
                    formData.append("newpass", document.getElementById("newpass").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                swal('Success',data,'success');
                document.getElementById("newpass").value = "";
                document.getElementById("Cnewpass").value = "";
            });
           };

    $scope.refreshPage = function () {
        window.location.reload();
    };

    $scope.setUrl = function(link){
        console.log(link);
        document.getElementById("viewer").src = link;
    };

    
$scope.showViewer = function (link){
    document.getElementById("viewer").src = link;
    document.getElementById("viewer").style.visibility = "visible";
    document.getElementById("viewer").style.position = "absolute";
    document.getElementById("viewer").style.height = "50rem";
    document.getElementById("viewer").style.width = "100%"; 
    
    document.getElementById("home").style.visibility = "hidden";
    document.getElementById("home").style.position = "absolute";
    document.getElementById("home").style.height = "1%";
    document.getElementById("home").style.width = "1%"; 
  }
    
});



app.controller('RMdataController', function ($scope, $http, $window) {  

	update_last_Activity();	
	
		var username = document.getElementById("username").innerHTML;
		var role = document.getElementById("_role").innerHTML;
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	 	function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			fetchusers();
		}); 
		};
    
	function fetchusers(){
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
	};

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'destination_tbl'})
    .then(function (res) {
       $scope.destinations = res.data != null ? res.data : 0;
    });

    function refreshDescription(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'destination_tbl'})
        .then(function (res) {
           $scope.destinations = res.data != null ? res.data : 0;
        });
    };

    $scope.fetchForBrand = function(_destination){
        $scope.brand2 = null;
        $scope.model2 = null;
        $scope.models = null;
        $scope.generalcode2 = null;
        $scope.generalcodes_ = null;
        $scope.fullcode2 = null;
        $scope.fullcodes_ = null;

        if(_destination != "" && _destination != null){
            var id = _destination != null && _destination != "" ? JSON.parse(_destination).Id : 0;
       $http.post('repairmanual/services/fetchdataForBrand.php', {tableName: 'brand_tbl', destinationId: id})
       .then(function (res) {
          $scope.brands_ = res.data != null ? res.data : 0;
       });
       }
       else{
        $scope.brands_ = "";
       }
    }

    $scope.fetchForModel = function(_brand){
        $scope.model2 = null;
        $scope.models = null;
        $scope.generalcode2 = null;
        $scope.generalcodes_ = null;
        $scope.fullcode2 = null;
        $scope.fullcodes_ = null;

        if(_brand != "" && _brand != null){
            var brandId_ = _brand != null && _brand != "" ?  JSON.parse(_brand).Id : 0;
           $http.post('repairmanual/services/fetchdataForModel.php', {tableName: 'model_tbl', brandId: brandId_})
           .then(function (res) {
              $scope.models_ = res.data != null ? res.data : 0;
           });
           }
           else{
            $scope.models_ = "";
           }
    }

    $scope.fetchForGeneralCode = function(_model){
        $scope.generalcode2 = null;
        $scope.generalcodes_ = null;
        $scope.fullcode2 = null;
        $scope.fullcodes_ = null;

        if(_model != "" && _model != null){
            var modelId_ = _model != null && _model != ""  ?  JSON.parse(_model).Id : 0;
           $http.post('repairmanual/services/fetchdataForGenralCode.php', {tableName: 'generalcode_tbl', modelId: modelId_})
           .then(function (res) {
              $scope.generalcodes_ = res.data != null ? res.data : 0;
           });
           }
           else{
            $scope.generalcodes_ = "";
           }
    };

    $scope.fetchForFullCode = function(_generalcode){
        $scope.fullcode2 = null;
        $scope.fullcodes_ = null;

        if(_generalcode != "" && _generalcode != null){
            var generalCodeId_ = _generalcode != null && _generalcode != ""  ?  JSON.parse(_generalcode).Id : 0;

           $http.post('repairmanual/services/fetchdataForFullCode.php', {tableName: 'fullcode_tbl', generalCodeId: generalCodeId_})
           .then(function (res) {
              $scope.fullcodes_ = res.data != null ? res.data : 0;
           });
           }
           else{
            $scope.fullcodes_ = "";
           }
    };

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'model_tbl'})
    .then(function (res) {
       $scope.models = res.data != null ? res.data : 0;
    });

    function refreshModel(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'model_tbl'})
        .then(function (res) {
           $scope.models = res.data != null ? res.data : 0;
        });
    };

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'brand_tbl'})
    .then(function (res) {
       $scope.brands = res.data != null ? res.data : 0;
    });

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'manual'})
    .then(function (res) {
       $scope.manuals = res.data != null ? res.data : 0;
	     angular.element(document).ready(function() { 
	   			 // Setup - add a text input to each footer cell
				$('#example tfoot th').each( function () {
					var title = $(this).text();
					$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
				} );
			 
				// DataTable
				var table = $('#example').DataTable({
					initComplete: function () {
						// Apply the search
						this.api().columns().every( function () {
							var that = this;
			 
							$( 'input', this.footer() ).on( 'keyup change clear', function () {
								if ( that.search() !== this.value ) {
									that
										.search( this.value )
										.draw();
								}
							} );
						} );
					}
				});
	      }); 
    });

    function refreshManual(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'manual'})
        .then(function (res) {
        $scope.manuals = res.data != null ? res.data : 0;
        });
    };

    function refreshBrand(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'brand_tbl'})
        .then(function (res) {
           $scope.brands = res.data != null ? res.data : 0;
        });
    };

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'generalcode_tbl'})
    .then(function (res) {
       $scope.generalcodes = res.data != null ? res.data : 0;
    });

    function refreshGeneralCode(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'generalcode_tbl'})
        .then(function (res) {
           $scope.generalcodes = res.data != null ? res.data : 0;
        });
    };

    $http.post('repairmanual/services/fetchdata.php', {tableName: 'fullcode_tbl'})
    .then(function (res) {
       $scope.fullcodes = res.data != null ? res.data : 0;
    });

    function refreshFullCode(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'fullcode_tbl'})
        .then(function (res) {
           $scope.fullcodes = res.data != null ? res.data : 0;
        });
    };

    $scope.addDestination = function(){
        $http({
            method: 'POST',
            url: "repairmanual/services/addDestination.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("name", document.getElementById("name").value);
                formData.append("description", document.getElementById("description").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshDescription();
            document.getElementById("name").value = "";
            document.getElementById("description").value = "";
            swal("Success", data , "success");
        });
				$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add Destination | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.addModel = function(){
        $http({
            method: 'POST',
            url: "repairmanual/services/addModel.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("brandId", document.getElementById("brandId").value);
                formData.append("name", document.getElementById("name2").value);
                formData.append("description", document.getElementById("description2").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshModel();
            document.getElementById("name2").value = "";
            document.getElementById("description2").value = "";
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add Model | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.addBrand = function(){
        $http({
            method: 'POST',
            url: "repairmanual/services/addBrand.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("destinationId", document.getElementById("destinationId").value);
                formData.append("name", document.getElementById("name3").value);
                formData.append("description", document.getElementById("description3").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshBrand();
            document.getElementById("name3").value = "";
            document.getElementById("description3").value = "";
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add Brand | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.addGeneralCode = function(){
        $http({
            method: 'POST',
            url: "repairmanual/services/addGeneralCode.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("modelId", document.getElementById("modelId").value);
                formData.append("name", document.getElementById("name4").value);
                formData.append("description", document.getElementById("description4").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshGeneralCode();
            document.getElementById("name4").value = "";
            document.getElementById("description4").value = "";
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add General Code | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };

    $scope.addFullCode = function(){
        $http({
            method: 'POST',
            url: "repairmanual/services/addFullCode.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("generalcodeId", document.getElementById("generalcodeId").value);
                formData.append("name", document.getElementById("name5").value);
                formData.append("description", document.getElementById("description5").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshFullCode();
            document.getElementById("name5").value = "";
            document.getElementById("description5").value = "";
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add Full Code | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };


    $scope.saveManual = function(){
        $http({
            method: 'POST',
            url: "repairmanual/services/addManual.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("destination", JSON.parse($scope.destination2).name);
                formData.append("brand", JSON.parse($scope.brand2).name);
                formData.append("model", JSON.parse($scope.model2).name);
                formData.append("generalcode", JSON.parse($scope.generalcode2).code);
                formData.append("productiondate", document.getElementById("productiondate2").value);
                formData.append("fullcode", JSON.parse($scope.fullcode2) != null ? JSON.parse($scope.fullcode2).code : "");
                formData.append("language", document.getElementById("language2").value);
                formData.append("link", document.getElementById("link2").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshManual();
            //document.getElementById("destination2").value = "";
            //document.getElementById("brand2").value = "";
            //document.getElementById("model2").value = "";
            //document.getElementById("generalcode2").value = "";
            //document.getElementById("productiondate2").value = "";
            //document.getElementById("fullcode2").value = "";
            //document.getElementById("language2").value = "";
            //document.getElementById("link2").value = "";
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add New Manual | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
    };


    $scope.deleteManual = function(Id){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "repairmanual/assets/img/delete.png",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $http.post('repairmanual/services/delete.php', {
                tableName: 'manual',
                id: Id
                }).then(function (response) {
                    refreshManual();
                    console.log(response.data);
                    swal("Success", "Data Removed Successfully!" , "success");
                });
				$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Remove Manual | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
              };
            });
     };

    $scope.deleteDescription = function(Id){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "repairmanual/assets/img/delete.png",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $http.post('repairmanual/services/delete.php', {
                tableName: 'destination_tbl',
                id: Id
                }).then(function (response) {
                    refreshDescription();
                    console.log(response.data);
                    swal("Success", "Data Removed Successfully!" , "success");
                });
				$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Remove Destination | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
              };
            });
     };

     $scope.deleteModel = function(Id){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "repairmanual/assets/img/delete.png",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $http.post('repairmanual/services/delete.php', {
                tableName: 'model_tbl',
                id: Id
                }).then(function (response) {
                    refreshModel();
                    console.log(response.data);
                    swal("Success", "Data Removed Successfully!" , "success");
                });
              };
			  $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Remove Model | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
            });
     };

     $scope.deleteBrand = function(Id){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "repairmanual/assets/img/delete.png",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $http.post('repairmanual/services/delete.php', {
                tableName: 'brand_tbl',
                id: Id
                }).then(function (response) {
                    refreshBrand();
                    console.log(response.data);
                    swal("Success", "Data Removed Successfully!" , "success");
                });
              };
			   $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Remove Brand | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
            });
     };

     $scope.deleteGeneralCode = function(Id){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "repairmanual/assets/img/delete.png",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $http.post('repairmanual/services/delete.php', {
                tableName: 'generalcode_tbl',
                id: Id
                }).then(function (response) {
                    refreshGeneralCode();
                    console.log(response.data);
                    swal("Success", "Data Removed Successfully!" , "success");
                });
              };
			   $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Remove General Code | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    }); 
            });
     };

     $scope.deleteFullCode = function(Id){
        swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this data!",
              icon: "repairmanual/assets/img/delete.png",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                $http.post('repairmanual/services/delete.php', {
                tableName: 'fullcode_tbl',
                id: Id
                }).then(function (response) {
                    refreshFullCode();
                    console.log(response.data);
                    swal("Success", "Data Removed Successfully!" , "success");
                });
              };
			  $http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Remove Full Code | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    });
            });
     };


     $scope.FetchManual =  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'manual', id: _id})
        .then(function (res) {
           $scope._id = res.data[0].Id;
           $scope._brand = res.data[0].brand;
           $scope._model = res.data[0].model;
           $scope._destination = res.data[0].destination;
           $scope._generalcode = res.data[0].general_code;
           $scope._fullcode = res.data[0].full_code;
           $scope.productiondate2 = res.data[0].production_date;
           $scope._link = res.data[0].link;
           $scope._language = res.data[0].language;
        });
    };

    $scope.FetchDescription =  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'destination_tbl', id: _id})
        .then(function (res) {
           $scope._id2 = res.data[0].Id;
           $scope._name2 = res.data[0].name;
           $scope._description2 = res.data[0].description;
        });
    };

    $scope.FetchModel =  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'model_tbl', id: _id})
        .then(function (res) {
           $scope._id3 = res.data[0].Id;
           $scope._name3 = res.data[0].name;
           $scope._description3 = res.data[0].description;
        });
    };
    
    $scope.FetchBrand =  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'brand_tbl', id: _id})
        .then(function (res) {
           $scope._id4 = res.data[0].Id;
           $scope._name4 = res.data[0].name;
           $scope._description4 = res.data[0].description;
        });
    };

    $scope.FetchGeneralCode =  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'generalcode_tbl', id: _id})
        .then(function (res) {
           $scope._id5 = res.data[0].Id;
           $scope._name5 = res.data[0].code;
           $scope._description5 = res.data[0].description;
        });
    };

    $scope.FetchFullCode=  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'fullcode_tbl', id: _id})
        .then(function (res) {
           $scope._id6 = res.data[0].Id;
           $scope._name6 = res.data[0].code;
           $scope._description6 = res.data[0].description;
        });
    };

    $scope.UpdateManual = function(id){
        $http({
            method: 'POST',
            url: "repairmanual/services/updateManual.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", id);
                formData.append("destination", document.getElementById("destination_").value);
                formData.append("brand", document.getElementById("brand_").value);
                formData.append("model", document.getElementById("model_").value);
                formData.append("generalcode", document.getElementById("generalcode_").value);
                formData.append("productiondate", document.getElementById("productiondate_").value);
                formData.append("fullcode", document.getElementById("fullcode_").value);
                formData.append("language", document.getElementById("language_").value);
                formData.append("link", document.getElementById("link_").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshManual();
            swal("Success", data , "success");
        });
    };


    $scope.UpdateDestination = function(id){
        $http({
            method: 'POST',
            url: "repairmanual/services/updateDescription.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", id);
                formData.append("description", document.getElementById("_description2").value);
                formData.append("name", document.getElementById("_name2").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshDescription();
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update Destination | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    });
    };

    $scope.UpdateModel = function(id){
        $http({
            method: 'POST',
            url: "repairmanual/services/updateModel.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", id);
                formData.append("description", document.getElementById("_description3").value);
                formData.append("name", document.getElementById("_name3").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshModel();
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update Model | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    });
    };


    $scope.UpdateBrand = function(id){
        $http({
            method: 'POST',
            url: "repairmanual/services/updateBrand.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", id);
                formData.append("description", document.getElementById("_description4").value);
                formData.append("name", document.getElementById("_name4").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshBrand();
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update Brand | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    });
    };

    $scope.UpdateGeneralCode = function(id){
        $http({
            method: 'POST',
            url: "repairmanual/services/updateGeneralCode.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", id);
                formData.append("description", document.getElementById("_description5").value);
                formData.append("name", document.getElementById("_name5").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshGeneralCode();
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update General Code | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    });
    };



    $scope.UpdateFullCode = function(id){
        $http({
            method: 'POST',
            url: "repairmanual/services/updateFullCode.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", id);
                formData.append("description", document.getElementById("_description6").value);
                formData.append("name", document.getElementById("_name6").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshFullCode();
            swal("Success", data , "success");
        });
		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Update Full Code | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
			    });
    };

});



app.controller('RMusersController', function ($scope, $http, $window) {
	
		update_last_Activity();	
	
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 
	
	setInterval(function(){		
		$http.post('services/getCurrentTime.php')
			.then(function (res) {
			   $scope.liveDateTime = res.data;
		}); 		
		update_last_Activity();
	},3000);
	
	  	     function update_last_Activity(){
		$http.post('services/updateLastActivity.php', {tableName: 'users'})
		.then(function (res) {
			console.log(res.data);
			fetchusers();
		}); 
		};
    
	function fetchusers(){
    $http.post('services/FetchData.php', {tableName: 'users'})
    .then(function (res) {
       $scope.users = res.data;
    }); 
	};
     
    $http.post('repairmanual/services/fetchdata.php', {tableName: 'users'})
    .then(function (res) {
    $scope.users = res.data;
		   	   angular.element(document).ready(function() { 
	   			 // Setup - add a text input to each footer cell
				$('#example tfoot th').each( function () {
					var title = $(this).text();
					$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
				} );
			 
				// DataTable
				var table = $('#example').DataTable({
					initComplete: function () {
						// Apply the search
						this.api().columns().every( function () {
							var that = this;
			 
							$( 'input', this.footer() ).on( 'keyup change clear', function () {
								if ( that.search() !== this.value ) {
									that
										.search( this.value )
										.draw();
								}
							} );
						} );
					}
				});
	      }); 
   });

    function refreshUsers(){
        $http.post('repairmanual/services/fetchdata.php', {tableName: 'users'})
        .then(function (res) {
        $scope.users = res.data;
       });
    };

    $scope.FetchUser =  function (_id){
        $http.post('repairmanual/services/fetchSingleData.php', {tableName: 'users', id: _id})
        .then(function (res) {
           $scope.id = res.data[0].Id;
           $scope.username = res.data[0].user_name;
		   $("#role_edit").val(res.data[0].role.split(",")).trigger("change");
        });
    };
 
    $scope.Adduser = function(){
		var selectedRoles = String($("#role").val());
        var Rolearray = selectedRoles.split(',');
		
        $http.post('repairmanual/services/fetchsingleUser.php', {tableName: 'users', username: document.getElementById("username").value})
        .then(function (res) {
          if(res.data.length > 0){
            swal({
                title: "Notice",
                text: "This Username is Already Exist!",
                icon: "warning",
                dangerMode: true,
              });
          }
          else{
            $http({
                method: 'POST',
                url: "repairmanual/services/adduser.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("fullname", document.getElementById("fullname").value);
                    formData.append("username", document.getElementById("username").value);
                    formData.append("password", document.getElementById("password").value);
                    formData.append("role", document.getElementById("role").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                refreshUsers();
                document.getElementById("fullname").value = "";
                document.getElementById("username").value = "";
                document.getElementById("password").value = "";
                document.getElementById("Cnewpasss").value = "";
                swal("Success", data , "success");
            });
          }
        });
    };

    $scope.updateUser = function(){
	    var selectedRoles = String($("#role_edit").val());
        var Rolearray = selectedRoles.split(',');
        $http({
            method: 'POST',
            url: "services/updateUser.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", document.getElementById("id_edit").value);
                formData.append("username", document.getElementById("username_edit").value);
                formData.append("role", Rolearray);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            fetchusers();
            swal("Success", data , "success");
        });
    };

    $scope.changePassword = function () {
        $http({
            method: 'POST',
            url: "repairmanual/services/changePassword.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("uname", document.getElementById("uname").value);
                formData.append("newpass", document.getElementById("newpass").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            swal('Success',data,'success');
            document.getElementById("newpass").value = "";
            document.getElementById("Cnewpass").value = "";
        });
       };

    $scope.ResetPassword = function(){
            $http({
                method: 'POST',
                url: "repairmanual/services/resetPassword.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("__id", document.getElementById("_id_").value);
                    formData.append("newPassword", document.getElementById("newpass_").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                refreshUsers();
                swal("Success", data , "success");
            });
    };



    $scope.deleteUser = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "repairmanual/assets/img/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('repairmanual/services/delete.php', {
                tableName: 'users',
                id: Id
                      }).then(function (response) {
                        refreshUsers();
                        console.log(response.data);
                        swal("Success", "User Deleted Successfully!" , "success");
                      });
            };
          });
    };

});

app.controller('TGMSController', function ($scope, $http, $window) {

    var username = document.getElementById("username").innerHTML;
    var role = document.getElementById("_role").innerHTML;

    $http.post('services/FetchData.php', {tableName: 'tblacessid'})
    .then(function (res) {
       $scope.licenses = res.data;
    }); 

        $http.post('services/FetchData.php', {tableName: 'users'})
        .then(function (res) {
           $scope.users = res.data;
        }); 

        $http.post('services/FetchData.php', {tableName: 'tblcasefile'})
        .then(function (res) {
           $scope.assignments = res.data;
        }); 

    function refreshLicense(){
        $http.post('services/FetchData.php', {tableName: 'tblacessid'})
        .then(function (res) {
           $scope.licenses = res.data;
        }); 
    };

    function refreshLicenseAssignmnet(){
        $http.post('services/FetchData.php', {tableName: 'tblcasefile'})
        .then(function (res) {
           $scope.assignments = res.data;
        }); 
    };

    $scope.SaveLicense = function(){
                $http({
                    method: 'POST',
                    url: "services/saveLicense.php",
                    processData: false,
                    transformRequest: function (data) {
                        var formData = new FormData();
                        formData.append("accessId", document.getElementById("accessId").value);
                        formData.append("startDate", document.getElementById("startDate").value);
                        formData.append("dueDate", document.getElementById("dueDate").value);
                        formData.append("status", document.getElementById("status").value);
                        formData.append("priority", document.getElementById("priority").value);
                        formData.append("description", document.getElementById("description").value);
                        return formData;
                    },
                    data: $scope.form,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function (data) {
                    refreshLicense();
                    document.getElementById("accessId").value = "";
                    document.getElementById("startDate").value = "";
                    document.getElementById("dueDate").value = "";
                    document.getElementById("status").value = "";
                    document.getElementById("priority").value = "";
                    document.getElementById("description").value = "";
                    swal("Success", data , "success");
                });
                $http.post('services/writeLog.php', {
                            log: "- username: " + username + " | role: " + role + " | Operation: Add TGMS License | DateTime: " + new Date() + " \n\n"
                            }).then(function (res) {
                               console.log(res.data);
                        }); 
    };

    $scope.deleteLicense = function(Id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "repairmanual/assets/img/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/deleteLicense.php', {
                tableName: 'tblacessid',
                id: Id
                      }).then(function (response) {
                        refreshLicense();
                        swal("Success", response.data , "success");
                      });
            };
          });
    };

    $scope.FetchLicense=  function (_id){
        $http.post('services/fetchsinglelicense.php', {tableName: 'tblacessid', id: _id})
        .then(function (res) {
            console.log(res.data);
           $scope._id = res.data[0].Id;
           $scope._accessId = res.data[0].access_id;
           $scope._startDate = res.data[0].start_date;
           $scope._dueDate = res.data[0].due_date;
           document.getElementById("_status").value = res.data[0].status;
           $scope._priority = res.data[0].priority;
           $scope._description = res.data[0].description;
        });
    };

    $scope.FetchLicenseAssignment =  function (_id){
        $http.post('services/fetchsinglelicenseassignment.php', {tableName: 'tblcasefile', id: _id})
        .then(function (res) {
            console.log(res.data);
           document.getElementById("_case_id").value = res.data[0].case_id;
           $scope._case_id = res.data[0].case_id;
           //document.getElementById("_user").value = res.data[0].username;
           $scope._user = res.data[0].username;
           //document.getElementById("_workStation").value = res.data[0].WorkStation;
           $scope._workStation = res.data[0].WorkStation;
           //document.getElementById("_os").value = res.data[0].OS;
           $scope._os = res.data[0].OS;
           //document.getElementById("_type").value = res.data[0].Cert_Type;
           $scope._type = res.data[0].Cert_Type;
           //document.getElementById("_license_issued").value = res.data[0].Cer_Issue;
           $scope._license_issued = res.data[0].Cer_Issue;
           //document.getElementById("_vpn").value = res.data[0].VPN_Type;
           $scope._vpn = res.data[0].VPN_Type;
           //document.getElementById("_location").value = res.data[0].Location;
           $scope._location = res.data[0].Location;
        });
    };

    $scope.updateLicense = function(_id){
        $http({
            method: 'POST',
            url: "services/updateLicense.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", _id);
                formData.append("accessId", $scope._accessId );
                formData.append("startDate", $scope._startDate);
                formData.append("dueDate", $scope._dueDate );
                formData.append("status", $scope._status );
                formData.append("priority", $scope._priority);
                formData.append("description", $scope._description);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshLicense();
            swal("Success", data , "success");
        });
    };

    $scope.SaveLicenseAssignmnet = function(){

        $http.post('services/fetchsinglelicenseassignment', {tableName: 'tblcasefile', id: _case_id})
        .then(function (res) {
            console.log(res.data);

        if(res.data.length <= 0 || res.data[0].case_id == _case_id){

        $http({
            method: 'POST',
            url: "services/saveLicenseAssignment.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("user", document.getElementById("user").value);
                formData.append("workStation", document.getElementById("workStation").value);
                formData.append("os", document.getElementById("os").value);
                formData.append("type", document.getElementById("type").value);
                formData.append("license_issued", JSON.parse(document.getElementById("license_issued").value).access_id);
                formData.append("status", JSON.parse(document.getElementById("license_issued").value).status);
                formData.append("description", JSON.parse(document.getElementById("license_issued").value).description);
                formData.append("start_date", JSON.parse(document.getElementById("license_issued").value).start_date);
                formData.append("due_date", JSON.parse(document.getElementById("license_issued").value).due_date);
                formData.append("vpn", document.getElementById("vpn").value);
                formData.append("location", document.getElementById("location").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshLicenseAssignmnet();
            document.getElementById("user").value = "";
            document.getElementById("workStation").value = "";
            document.getElementById("os").value = "";
            document.getElementById("type").value = "";
            document.getElementById("license_issued").value = "";
            document.getElementById("vpn").value = "";
            document.getElementById("location").value = "";
            swal("Success", data , "success");
        });

		$http.post('services/writeLog.php', {
				    log: "- username: " + username + " | role: " + role + " | Operation: Add License Assignment | DateTime: " + new Date() + " \n\n"
				    }).then(function (res) {
					   console.log(res.data);
                }); 
            }
            else{
                swal("Validation Failed", "This License is already Assigned, Clear it first." , "error");
            }
        });
    };

    $scope.deleteLicenseAssignment = function(Id){
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this user!",
            icon: "repairmanual/assets/img/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/deleteLicenseAssignment.php', {
                tableName: 'tblcasefile',
                id: Id
                      }).then(function (response) {
                        refreshLicenseAssignmnet();
                        swal("Success", response.data , "success");
                      });
            };
          });
    };

    $scope.UpdateLicenseAssignmnet = function(){

        var accessId = JSON.parse(document.getElementById("_license_issued").value);
        console.log(accessId.access_id);

        $http.post('services/fetchsinglelicenseassignmentByissued', {tableName: 'tblcasefile', id: accessId.access_id})
        .then(function (res) {
            console.log(res.data);

        if(res.data.length <= 0 || res.data[0].Cer_Issue == accessId.access_id){
        $http({
            method: 'POST',
            url: "services/updateLicenseAssignment.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("id", document.getElementById("_case_id").value);
                formData.append("user", document.getElementById("_user").value);
                formData.append("workStation", document.getElementById("_workStation").value);
                formData.append("os", document.getElementById("_os").value);
                formData.append("type", document.getElementById("_type").value);
                formData.append("license_issued", JSON.parse(document.getElementById("_license_issued").value).access_id);
                formData.append("status", JSON.parse(document.getElementById("_license_issued").value).status);
                formData.append("description", JSON.parse(document.getElementById("_license_issued").value).description);
                formData.append("start_date", JSON.parse(document.getElementById("_license_issued").value).start_date);
                formData.append("due_date", JSON.parse(document.getElementById("_license_issued").value).due_date);
                formData.append("vpn", document.getElementById("_vpn").value);
                formData.append("location", document.getElementById("_location").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            refreshLicenseAssignmnet();
            swal("Success", data , "success");
        });
        }
        else{
            swal("Validation Failed", "This License is already Assigned, Clear it first." , "error");
        }
    });
    };

});

    
