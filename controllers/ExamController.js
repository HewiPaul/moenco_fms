/// <reference path="../dist/js/angular.js" />

var app = angular.module("ExamManagment", ["ngRoute", "angularUtils.directives.dirPagination"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "views/examHome.htm",
                controller: "mainController"
            })
            .when("/register", {
                templateUrl: "views/candidateRegistration.htm",
                //templateUrl: "views/exam.htm",
                controller: "mainController"
            })
            .when("/exam", {
                templateUrl: "views/exam.php",
                controller: "mainController"
            })
    });

var curDate = new Date();
var curDay = new Date().getDate();
var curDateMonth = new Date().getMonth() + 1; // we must have to add 1 to get the correcr month
var curDateYear = new Date().getFullYear();
var now = curDateYear + '-' + curDateMonth + '-' + curDay;

app.run(['$http', function ($http) {
            
}]);

app.controller('mainController', function ($scope, $http, $window) {

    var Id = 0;
    var answerSubmited = 0;
    
    $http.post('services/FetchData.php', {
        tableName: 'examsession'
        }).then(function (response) {
        //console.log(response.data);
        $scope.sessions = response.data;
    });
    console.log(JSON.parse(sessionStorage.getItem("questions")));
    $scope.questions = JSON.parse(sessionStorage.getItem("questions"));

    $http.post('services/getLastApplicant.php', {tableName: 'candidates'})
    .then(function (res) {
       if(res.data != null && res.data.Id > 0)
            Id = (res.data.Id * 1) + 1;
        else
            Id = 1;
       //$scope.notice = res.data[0];
    });

    $scope.SaveRegistration = function () {
        $http({
            method: 'POST',
            url: "services/candidateRegistration.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("uniqueId","APP-" + Id)
                formData.append("fullname", $scope.fname + ' ' + $scope.lname + ' ' + $scope.gname);
                formData.append("age", $scope.age);
                formData.append("gender", document.getElementById("gender").value);
                formData.append("email", $scope.email);
                formData.append("phone", $scope.phone);
                formData.append("session", document.getElementById("session").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            $http.post('services/fetchExamQuestions.php', {
                tableName: 'examsession',
                examSessionId: document.getElementById("session").value
                }).then(function (response) {
                console.log(response.data);
                sessionStorage.setItem("questions", JSON.stringify(response.data));
            });      
            swal({
                title: "Success",
                text: data + " Your Applicant ID is: APP-" + Id,
                icon: "success"
              });   
              setTimeout(() => { 
                location.href = "#/exam";
                }, 1000);
        });
    };

    $scope.TakeExam = function(){
        var examSessionId = document.getElementById("sessionId").value;
        var candidateId = document.getElementById("myId").value;
        var taken = null;



        $http.post('services/fetchsingledatabycandidateIdy.php', {
            tableName: 'candidates',
            id: candidateId
            }).then(function (response) {
               console.log(response.data);
               var ApplicantSessionId = response.data.length > 0 ? response.data[0].examSessionId : 0;
               if(response.data.length <= 0 || examSessionId != ApplicantSessionId){
                swal({
                    title: "Validation Failed",
                    text: "Sorry, You did'nt register for this exam session.",
                    icon: "error"
                  });
               }else if(ApplicantSessionId == examSessionId){
                $http.post('services/checkExamTaken.php', {
                    tableName: 'examrecord',
                    examSessionId: examSessionId,
                    candidateId: candidateId
                    }).then(function (response) {
                    //console.log(response.data);
                    taken = response.data;
        
                    if(taken != null && taken.length > 0){
                        swal({
                            title: "Validation Failed",
                            text: "Sorry, You Already Took This Exam!",
                            icon: "error"
                          });
                    }
                    else{
                    $http.post('services/fetchsingledatabyuniqueId.php', {
                        tableName: 'candidates',
                        id : $scope.myId
                        }).then(function (response) {
                        console.log(response.data);
                        if(response.data != "0" && response.data != 0 && response.data != null){
                            $http.post('services/fetchExamQuestions.php', {
                                tableName: 'examsession',
                                examSessionId: examSessionId
                                }).then(function (response) {
                                console.log(response.data);
                                sessionStorage.setItem("questions", JSON.stringify(response.data));
                            });
                            swal({
                                title: "Success",
                                text: "Exam Entrance Successfull!",
                                icon: "success"
                              });
                            setTimeout(() => { 
                                document.location.href = "#/exam";
                            }, 1000);
                        }
                        else{
                            swal({
                                title: "Validation Failed",
                                text: "Your Not Registered, Please Get Registered.",
                                icon: "error"
                              });
                        }
                    });
                   }
                });
               }
               else{
                swal({
                    title: "Validation Failed",
                    text: "Your Not Registered, Please Get Registered.",
                    icon: "error"
                  });
               }
            });
        //alert(examSessionId);
    }; 

    $scope.SubmitAnswers = function () {

        var examSessionId = 0;
        var candidateId = document.getElementById("candidateId").value;

            if(answerSubmited != 0){
                swal({
                    title: "Validation",
                    text: "Answers Already Submited For this Exam!",
                    icon: "error"
                  });
            }
            else{
                $http.post('services/fetchSingleDataByUniqueIdChecker.php', {
                    tableName: 'candidates',
                    id : candidateId
                    }).then(function (response) {
                    //console.log(response.data);
                    examSessionId = response.data[0].examSessionId;

                    $http.post('services/checkExamTaken.php', {
                        tableName: 'examrecord',
                        candidateId : candidateId,
                        examSessionId : examSessionId
                        }).then(function (response) {
                        //console.log(response.data);
                        if(response.data.length <= 0){
                            submit();
                            answerSubmited = 1;
                        }
                        else{
                            swal({
                                title: "Validation",
                                text: "Sorry, You Already Took This Exam!",
                                icon: "error"
                              });
                        }
                    });

                });
            };

        function submit(){
            var result = 0;
            var right = 0;
            var wrong = 0;
        $scope.questions.forEach(element => {
            var selectedValue = $('input[name="q_answer'+ element.id +'"]:checked').val();
            //alert("q_answer"+ element.id +": " + selectedValue);
            $http({
                method: 'POST',
                url: "services/submitAnswer.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("sessionId", examSessionId);
                    formData.append("questionSetId", element.questionSetId);
                    formData.append("questionId", element.id);
                    formData.append("candidateId", document.getElementById("candidateId").value);
                    formData.append("question", element.question);
                    formData.append("answer", element.answer);
                    formData.append("candidate_answer", selectedValue);
                    formData.append("isRight", selectedValue == element.answer ? 1 : 0);
                    if(selectedValue == element.answer){
                        right = right + 1;
                        result = right;
                    }
                    else{
                        wrong = wrong + 1;
                    }
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                var myhtml = document.createElement("div");
                myhtml.innerHTML = data + '<br><br>' + "<span style='font-weight:bold'>You Got: </span>" + result + ' / ' + $scope.questions.length + '<br>' + "<span style='font-weight:bold'> Correct Answer (&#10003;): </span>" + right + '<br>' + "<span style='font-weight:bold'> Wrong Answer (&#120;): </span>" + wrong;
                swal({
                    html:true,
                    title: "Success",
                    content: myhtml, 
                    icon: "success"
                  });
                document.getElementById("showResult").disabled = false;
            });
        });
    };
    };

    $scope.showResult = function(){
        var uniqueId = document.getElementById('candidateId').value;
        $http.post('services/fetchExamResultDetailByCandidateId.php', {
            tableName: 'examrecord',
            id : uniqueId
            }).then(function (response) {
            //console.log(response.data);
            var rows = response.data;
            var result = 0;
            var right = 0;
            var wrong = 0;
            rows.forEach(element => {
                result = result + element.isRight * 1;
                right = result;
                wrong = $scope.questions.length * 1 - result;
            });
            var myhtml = document.createElement("div");
            myhtml.innerHTML = '<br>' + "<span style='font-weight:bold'>You Got: </span>" + result + ' / ' + $scope.questions.length + '<br>' + "<span style='font-weight:bold'> Correct Answer (&#10003;): </span>" + right + '<br>' + "<span style='font-weight:bold'> Wrong Answer (&#120;): </span>" + wrong;
            swal({
                html:true,
                title: "Exam Result",
                content: myhtml, 
                icon: "info"
              });
            examSessionId = response.data[0].examSessionId;
        });
    };

});

