<?php session_start(); $session_value = (isset($_SESSION['error']))?$_SESSION['error']:''; ?>
<!DOCTYPE html>
<html lang="en" ng-app="FSManagment">
<head>
<title>MOENCO File Shareing Sytem</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/HR Logo.png" type="image/x-icon">
<!--===============================================================================================-->	
<link href="./images/moenco_logo.png" rel="icon" type="image/png">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor_/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor_/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor_/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="dist/css/util.css">
	<link rel="stylesheet" type="text/css" href="dist/css/main.css">
<!--===============================================================================================-->
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/fontawesome-all.css">
</head>
<body style="height:100%; overflow-y: hidden">
	<div class="size1 bg0 where1-parent col-md-12">
		<!-- Coutdown -->
		<div class="flex-c-m bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('images/pdf-sharing.png');">
			<div style="width:100%"  class="cd100 js-tilt row col-md-12">
			    <div class="row col-md-12">
						<form class="row col-md-12" action="services/Login.php" method="post" name="login">
								<h1 style="color:rgb(250, 243, 244);">Login</h1>
								<br><br>
								<div class="form-group">
								<input class="form-control" type="text" name="username" placeholder="Username" required />
								</div>
								<br>
								<div class="form-group">
								<input class="form-control" type="password" name="password" placeholder="Password" required />
								</div>
								<br>
								<div class="form-group">
								<input style="width: 200px" class="btn btn-success" name="submit" type="submit" onClick="showAlert()" value="Login" />
								</div>
							</form>
				</div>
			</div>
		</div>
		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">
			<!--<div style="width:100%;">
				<img class="img img-responsive" src="images/logo.jpg" alt="LOGO">
			</div>-->

			<div style="text-align:center" class="p-t-50 p-b-60">
				<form class="contact100-form validate-form">
					<img class="img img-responsive" src="images/inchcape2.png"/>
					<!--<h4 style="color:rgb(133, 128, 3); margin: 20px">Employee Managment System</h4>-->
				</form>
			</div>
		</div>
	</div>


    <script src="dist/js/sweetAlert.js"></script>
	<script>
	message();
	function message() {
	   var content = document.createElement('div');
       var error = '<?php echo $session_value; ?>';
       if(error == "success"){
        content.innerHTML = 'Login Succesfull, <strong> WelCome! </strong>';
        swal({
            title: "Success",
            content: content,
            icon: "success",
            button: false
        });
       	setTimeout(function(){
          var unsetSession = '<?php unset($_SESSION['error']);?>';
          window.location.href = 'dashboard.php';
         }, 1500);
        }
       else if(error == "failed"){
         content.innerHTML = 'Invalid <strong> Username </strong> or <strong> Password </strong> Used!';
       	 swal({
          title: "Invalid Login!",
          content: content,
          text: "",
          icon: "warning",
          button:false
          });
       setTimeout(function(){
       var unsetSession = '<?php unset($_SESSION['error']);?>';
       window.location.href = 'login.php';
       }, 1500);
       }
	};
</script>

<!--===============================================================================================-->	
	<script src="vendor_/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor_/bootstrap/js/popper.js"></script>
	<script src="vendor_/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor_/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor_/countdowntime/moment.min.js"></script>
	<script src="vendor_/countdowntime/moment-timezone.min.js"></script>
	<script src="vendor_/countdowntime/moment-timezone-with-data.min.js"></script>
	<script src="vendor_/countdowntime/countdowntime.js"></script>
	<script>
		$('.cd100').countdown100({
			/*Set Endtime here*/
			/*Endtime must be > current time*/
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 7,
			endtimeHours: 18,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
			// ex:  timeZone: "America/New_York"
			//go to " http://momentjs.com/timezone/ " to get timezone
		});
	</script>
<!--===============================================================================================-->
	<script src="vendor_/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
    <script src="dist/js/sweetAlert.js"></script>
	<script src="dist/js/angular.js"></script>
	<script src="dist/js/angular.min.js"></script>
	<script src="dist/js/angular.js"></script>
	<script src="dist/js/angular-route.js"></script>
	<script src="dist/js/dirPagination.js"></script>
    <script src="controllers/PageController.js"></script>  
</body>
</html>