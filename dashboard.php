<?php
include("services/auth.php");
?>

<!DOCTYPE html>
<html lang="en" xmlns:ng="http://angularjs.org" ng-app="FSManagment">
<head>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Repair Manual Web App For MOENCO Inchcape.">
    <meta name="author" content="Hewi Paul">
  	<title>MOENCO File Shareing Sytem</title>
  	<!-- Tell the browser to be responsive to screen width -->
  	<!--eta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">-->
    <link href="./images/moenco_logo.png" rel="icon" type="image/png">
  	<!-- Bootstrap 3.3.7 -->
  	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  	<!-- Font Awesome -->
  	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  	<!-- Ionicons -->
  	<link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  	<!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  	<!--[if lt IE 9]>
  	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  	<![endif]-->

  	<!-- Google Font
  	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">-->

  	<style type="text/css">
  		.mt20{
  			margin-top:20px;
  		}
      .bold{
        font-weight:bold;
      }

     /* chart style*/
      #legend ul {
        list-style: none;
      }

      #legend ul li {
        display: inline;
        padding-left: 30px;
        position: relative;
        margin-bottom: 4px;
        border-radius: 5px;
        padding: 2px 8px 2px 28px;
        font-size: 14px;
        cursor: default;
        -webkit-transition: background-color 200ms ease-in-out;
        -moz-transition: background-color 200ms ease-in-out;
        -o-transition: background-color 200ms ease-in-out;
        transition: background-color 200ms ease-in-out;
      }

      #legend li span {
        display: block;
        position: absolute;
        left: 0;
        top: 0;
        width: 20px;
        height: 100%;
        border-radius: 5px;
      }
    </style>
</head>

<body id="body" class="hold-transition skin-purple sidebar-mini" onload="disableContextMenu();" oncontextmenu="return false"> 
    <div id="body" class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="#/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img class="img img-responsive" src="images/moenco.png"></span>
          <!-- logo for regular state and mobile devices -->
          <span style="font-weight:bold" class="logo-lg"><b></b> MOENCO</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
    
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="index.html" onclick="location.href='index.html'" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-tv"></i> Portal</a>
              <li>
              <li class="dropdown user user-menu">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="images/profile.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs">Hi, <?php echo $_SESSION['fsm_username']; ?></span>
                </a>
				<div style="visibility:hidden; display:none">
				<label id="username"><?php echo $_SESSION['fsm_username']; ?></label>
				<label id="_role"><?php echo $_SESSION['role']; ?></label>
				</div>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="images/profile.jpg" class="img-circle" alt="User Image">
    
                    <p>
                    <?php echo $_SESSION['fsm_username']; ?>
                      <small>Member since 4/18/2020</small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <!--<div class="pull-left">
                      <a href="#/changePassword" data-toggle="modal" class="btn btn-default btn-flat" id="admin_profile">Update</a>
                    </div>-->
                    <div style="width:100%; text-align:center">
                      <a href="services/logout.php" class="btn btn-primary btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>


      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="images/profile.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $_SESSION['fsm_username']; ?></p>
              <a><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">REPORTS</li>
            <li class=""><a href="#/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="header">MANAGE</li>
            <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'RMAdministrator') !== false|| strpos($_SESSION['role'], 'RMUser') !== false){
            ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cog"></i>
                <span>Repair Manuals</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/repairManual'"><i class="fa fa-file"></i> <span> Manuals</span></a></li>
                <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'RMAdministrator') !== false){
                ?>              
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/data_managment'"><i class="fa fa-car"></i> <span> Manage Data </span></a></li>
                <?php
                }
                ?>
                <!--<li><a href="javascript:window.location.reload(true)" onclick="location.href='#/user_managment'"><i class="fa fa-circle-o"></i> <span> Manual Users </span></a></li>  
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/RMsetting'"><i class="fa fa-circle-o"></i> <span> Manual Setting </span></a></li>-->          

                </ul>
            </li>
            <?php
                }
            ?>
            <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'FileAdministrator') !== false || strpos($_SESSION['role'], 'FileUser') !== false){
            ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i>
                <span>File Sharing</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php
                   if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'FileAdministrator') !== false){
                ?>              
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/addCatagory'"><i class="fa fa-list"></i> <span> Add Catagory </span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/fileManager'"><i class="fa fa-upload"></i> <span>New File</span></a></li>
                <?php
                    }
                ?>
                <li><a href="#/sharedFiles"><i class="fa fa-file-text"></i><span> Shared Files</span> </a></li> 
                </ul>
            </li>
            <?php
                }
            ?>
            <?php
                if(strpos($_SESSION['role'], 'SuperAdministrator') !== false || strpos($_SESSION['role'], 'ExamAdministrator') !== false){
            ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i>
                <span>Examination</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/questionSet'"><i class="fa fa-circle-o"></i> <span> Create Questions Set </span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/newQuestion'"><i class="fa fa-circle-o"></i> <span> Add New Question </span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/newExamSession'"><i class="fa fa-circle-o"></i> <span> Create Exam Session </span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/registerCandidate'"><i class="fa fa-circle-o"></i> <span> Register Candidate</span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/examSetting'"><i class="fa fa-circle-o"></i> <span> Exam Setting</span></a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="ion ion-pie-graph"></i>
                <span>Exam Reports</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/examReport'"><i class="fa fa-circle-o"></i> <span> Exam Report Per ES</span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/examReportPerapp'"><i class="fa fa-circle-o"></i> <span> Exam Report APP</span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/questionPerSessionReport'"><i class="fa fa-circle-o"></i> <span> Questions Per Session</span></a></li>
              </ul>
            </li>

            <?php
                }
            ?>
		 <?php
			 if(strpos($_SESSION['role'], 'SuperAdministrator') !== false){
		   ?>
       <li class="treeview">
              <a href="#">
                <i class="fa fa-certificate"></i>
                <span>TGMS</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/licenses'"><i class="fa fa-certificate"></i> <span> Lisences </span></a></li>
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/licenseAssignment'"><i class="fa fa-users"></i> <span> Lisences Assignments </span></a></li>
                <!--<li><a href="javascript:window.location.reload(true)" onclick="location.href='#/changePassword'"><i class="fa fa-circle-o"></i> <span> Change Password </span></a></li>-->
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-cog"></i>
                <span>Setting</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="javascript:window.location.reload(true)" onclick="location.href='#/users'"><i class="fa fa-users"></i> <span> Manage User </span></a></li>
                <!--<li><a href="javascript:window.location.reload(true)" onclick="location.href='#/changePassword'"><i class="fa fa-circle-o"></i> <span> Change Password </span></a></li>-->
              </ul>
            </li>
            <?php
                }
             ?>
            <!--<li class="header">PRINTABLES</li>
            <li><a href="#/"><i class="fa fa-clock-o"></i> <span>Schedule</span></a></li>-->
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

        <!-- Content Wrapper. Contains page content -->
      <div style="padding:10px" class="content-wrapper">
      <div class="row">

        <ng-view>
        
        </ng-view>

      </div>
      </div>


      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>@<a href="http://www.moencoethiopia.com.et" target="#">MOENCO</a></b>
        </div>
        <strong>Copyright &copy; 2020 FMS, File Managment System - <span style="color: rgb(148, 1, 1)">MOENCO</span></strong>
    </footer>

</div>
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="bower_components/raphael/raphael.min.js"></script>
<script src="bower_components/morris.js/morris.min.js"></script>
<!-- ChartJS -->
<script src="bower_components/chart.js/Chart.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/pdfObject.js"></script>
<script src="dist/js/demo.js"></script>
<script src="dist/js/sweetAlert.js"></script>
<script src="dist/js/select2.min.js"></script> 
<script src="dist/js/angular.js"></script>
<script src="dist/js/angular.min.js"></script>
<script src="dist/js/angular.js"></script>
<script src="dist/js/angular-route.js"></script>
<script src="dist/js/dirPagination.js"></script>
<script src="controllers/PageController.js"></script>  
<script>
  $(function () {
    $('#example1').DataTable({
      responsive: true
    })
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  });

	document.addEventListener('contextmenu', event => event.preventDefault());
	window.addEventListener('contextmenu', event => event.preventDefault());
	//document.addEventListener('keydown', disableContextMenu());
	//window.addEventListener('keydown', disableContextMenu());
	
	function disableContextMenu()
            {
                //window.frames["viewer"].document.oncontextmenu = function(){alert("No way!"); return false;};   
                // Or use this
                $(document.getElementById('body').contentWindow.document).keydown(function(e){
                      if(e.keyCode == 123 || e.keyCode == 121 || e.keyCode == 120 || e.keyCode == 119){
                        //122 f11-full screen, 38-up arrow, 40-down arrow
                        return false;
                    }
                });
                 //document.getElementById("body").contentWindow.document.oncontextmenu = function(){swal("Restriction Warning" , "Clicks are Locked on this page.", "error"); return false;};
                 //document.getElementById("viewer").contentWindow.document.click = function(){swal("Restriction Warning" , "Clicks are Locked on this page.", "error"); return false;};    
            }; 
</script>

<script>
$(function(){
  /** add active class and stay opened when selected */
  var url = window.location;

  // for sidebar menu entirely but not cover treeview
  $('ul.sidebar-menu a').filter(function() {
     return this.href == url;
  }).parent().addClass('active');

  // for treeview
  $('ul.treeview-menu a').filter(function() {
     return this.href == url;
  }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
  
});
</script>
<script>

$(function(){
	//Date picker
  $('#datepicker_add').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })
  $('#datepicker_edit').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd'
  })

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )
  
});
</script>

</body>
</html>