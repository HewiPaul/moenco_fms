<?php
    require_once 'db.php';
    $data = json_decode(file_get_contents("php://input"));
    $_tableName = $data->tableName;

	$query = $conn->query("SELECT can.uniqueId, can.fullname, can.gender, can.age, can.phone, er.examSessionId, ses.sessionName, er.question, er.answer, er.candidate_answer, er.isRight  FROM `examrecord` er, `candidates` can, `examsession` ses WHERE er.candidateId = can.uniqueId AND er.examSessionId = ses.id") or die(mysqli_error());
	$data = array();
 
	while($row = $query->fetch_array()){
		$data[] = $row;
	}
	echo json_encode($data);
?>