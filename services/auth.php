<?php
// 10 mins in seconds
$inactive = 7200; 
ini_set('session.gc_maxlifetime', $inactive); // set the session max lifetime to 2 hours

session_start();

if(!isset($_SESSION["fsm_username"])){
    header("Location: login.php");
}

if (isset($_SESSION['expire']) && (time() - $_SESSION['expire'] > $inactive)) {
    // last request was more than 2 hours ago
    session_unset();     // unset $_SESSION variable for this page
    session_destroy();   // destroy session data
    header("Location: login.php");
}
$_SESSION['expire'] = time(); // Update session
?>