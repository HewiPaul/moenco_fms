<?php
 // create Imagick object
 $imagick = new \Imagick();
 // Reads image from PDF
 $imagick->readImage('sample.pdf');
 // Writes an image or image sequence Example- converted-0.jpg, converted-1.jpg
 $imagick->writeImages('converted.jpg', false);
?>