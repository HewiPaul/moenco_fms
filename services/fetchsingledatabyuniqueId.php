<?php
    require_once 'db.php';
    $data = json_decode(file_get_contents("php://input"));
    $_tableName = $data->tableName;
    $Id = $data->id;

	$stmt = $conn->prepare("SELECT can.uniqueId as uniqueId, can.fullname as fullname, ses.sessionName as sessionName FROM candidates can , examsession ses WHERE can.uniqueId ='$Id' AND can.examSessionId = ses.id LIMIT 1");
	$stmt->execute();
	$data = $stmt->get_result()->fetch_assoc();

	$value = $data != null ? $data['uniqueId'] : "";
	$value2 = $data != null ? $data['fullname'] : "";
	$value3 = $data != null ? $data['sessionName'] : "";

	session_start();
	$_SESSION['candidateId'] = $value;
	$_SESSION['candidateName'] = $value2;
	$_SESSION['sessionName'] = $value3;

	echo json_encode($value);
?>