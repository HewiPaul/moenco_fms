<?php
    require_once 'db.php';
    $data = json_decode(file_get_contents("php://input"));
    $_tableName = $data->tableName;

	$query = $conn->query("SELECT can.id, can.uniqueId, can.fullname, can.gender, can.age, can.phone, er.examSessionId, ses.sessionName, SUM(er.isRight) as result, COUNT(er.examSessionId) as totalQuez FROM `examrecord` er, `candidates` can, `examsession` ses WHERE er.candidateId = can.uniqueId AND er.examSessionId = ses.id GROUP BY er.candidateId ORDER BY result DESC") or die(mysqli_error());
	$data = array();
 
	while($row = $query->fetch_array()){
		$data[] = $row;
	}
	echo json_encode($data);
?>