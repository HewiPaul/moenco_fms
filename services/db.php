<?php
    header('Content-type: text/html; charset=utf-8');
	define('db_host', 'localhost');
	define('db_user', 'root');
	define('db_pass', '');
	define('db_name', 'fmsdb');
 
    $conn = new mysqli(db_host, db_user, db_pass, db_name);
    mysqli_set_charset($conn,"utf8");
	if(!$conn){
		die("Fatal Error: Can't connect to database");
    }
?>
